# Georges Bataille: saber y transgresión

* Entidad: 10
* Asignatura: 63047
* Grupo: T124
* Créditos: 4
* Tipo: Curso
* Campo: Filosofía de la cultura
* Profesor(es): Dr. Ignacio González Díaz de la Serna

Aquí está el respaldo del pad: [https://pad.kefir.red/p/63047_t118_curso_georges-bataille-saber-y-transgre](https://pad.kefir.red/p/63047_t118_curso_georges-bataille-saber-y-transgre).
