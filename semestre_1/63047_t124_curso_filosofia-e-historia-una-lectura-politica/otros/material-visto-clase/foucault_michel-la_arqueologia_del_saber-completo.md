# El análisis arqueológico

\vskip -3em

## Sobre la introducción y la tercera parte de *La arqueología del saber*[^1]

\vskip 6em

<!-- Contextualización -->

\noindent Un año después de las revueltas estudiantiles, en 1969, se funda la
Universidad de Vincennes, un proyecto creado desde la agitación estudiantil
con el fin de responder a sus demandas. Entre las personas que se sumarían hay 
un filósofo nacido en Poitiers de poco más de cuarenta años (Castro, 2014). 
Su nombre es Paul-Michel Foucault y entre su obra publicada figuran tres libros 
que le han dado renombre: *Historia de la locura*, *El nacimiento de la clínica* 
y *Las palabras  y las cosas*. En este año no solo asume la jefatura del 
Departamento de Filosofía, también publica un libro que pretende abordar las 
metodologías implícitas en sus otras obras: *La arqueología del saber* 
(Castro, 2014).

<!-- Introducción -->

\vskip 1em\noindent Entre los objetivos de esta obra no solo está el intento de
darle coherencia a las metodologías que Foucault empleó en sus otros trabajos.
También existe la pretensión de revisar los métodos que se han empleado
en el quehacer historiográfico y, lo que él denomina, sus «sujeciones
antropológicas». La dificultad de esta intención es ya sabida de antemano,
porque esta tarea implica vérselas con grandes temas como el ser humano, la
conciencia, el origen y el sujeto. Su proceso será cauteloso —aunque también
varias veces confuso debido a que no tiene la estructura convencional de una
investigación, donde se avanza a través de la definición de conceptos. Es este
modo de andar y lo que se encontrará en el camino por lo que esta obra ocupará 
un lugar en la historia de la filosofía.

¿Cómo comienza este recorrido? En un primer momento entre una distinción general
de lo que está implícito en el quehacer historiográfico «tradicional» y cuya
«arqueología» en constitución no sigue. 

El historiador en su día a día delimita su investigación en periodos prolongados 
de tiempo, con los cuales poco a poco empezará a realizar distinciones con el 
propósito de encontrar una continuidad en una serie tan dispar de 
acontecimientos. Con sus instrumentos y conforme avanza, estas distinciones se 
irán generalizando hasta al fin dar con un trabajo que describe nuestro pasado 
como una totalidad, que encuentra en las profundidades de la historia el origen 
y el *telos* de la humanidad o que hace de nuestra historia un relato vivo, 
*continuo* y abierto: una historia centrada en el sujeto.

Aunque en un primer momento su método no sea cuestionable, la historia así 
producida contempla un conjunto de supuestos que tienen que comenzar a 
cuestionarse. El historiador sin ninguna clase de justificación asiente en:

1. La posibilidad de constituir un sistema homogéneo a partir del conjunto de
   acontecimientos de un determinado espacio y tiempo.
2. La historicidad como eje para al resto de las estructuras, como las 
   económicas, sociales, políticas o institucionales.
3. La historia como un modelo que se conforma de grandes unidades 
   espacio-temporales.

Lo que el historiador considera como dado más bien tiene su fundamento en
las filosofías de la historia que plantean la posibilidad de construir un
sentido, la relatividad del conocimiento histórico o la historia como teleología
racional. Lo que este sujeto da como entendible de suyo en realidad implica una
apertura de problemas metodológicos donde su visibilización erige una función
conservadora. En la negación de la crítica a sus métodos el investigador
denuncia la intención de destruir la historia: su conversión en instrumento
para otros fines o para catalogarla como herramienta inútil. Sin embargo, lo 
que el historiador defiende como *la* historia no es sino *una* historia 
«global», un discurso histórico que más sobresale por su pretendida 
sistematicidad centrada en la conciencia del sujeto y devenida en ideología, que 
en una práctica discursiva caracterizada por su cientificidad.

Los descentramientos que han permitido evidenciar el carácter problemático del
quehacer histórico pueden remontarse a Marx, la genealogía nietzscheana, el
psicoanálisis, la lingüística y la etnología. Y aunque el análisis arqueológico
asume críticamente lo dicho por todos ellos, su punto de partida inmediato son
las historias de las ideas, de la filosofía, de las ciencias y de la literatura.

Lo que caracteriza a estas historias es su carácter *discontinuo*. Estas otras 
historias —otredad porque han sido pasadas por alto por la mayoría de los 
historiadores— ponen su atención en las rupturas, las interrupciones, las
contradicciones o faltas de coherencia. Su eje analítico no pretende por ello
darle fin a la historia, sino dotarla de desplazamientos para llevar a cabo
una transformación teórica del quehacer historiográfico que le erradique
complicidades imaginarias y así sea posible constituir *una* historia «general».

Este discurso histórico se niega a hacer de la historia la memorización de
monumentos pasados. En su lugar, transforma a la historia en una masa de 
documentos inseparables de la sociedad que no es materia inerte, sino un tejido
que describe relaciones. La variabilidad de relaciones abre a la historia a
una multiplicidad de métodos de análisis sin la obligación de haber surgido 
del quehacer historiográfico. Con esto se obtiene una serie de criterios que
ayudarán al historiador a estipular la legitimidad de cada una de las relaciones
para poder delinear las que caracterizan a un conjunto documental.

Aquí es donde adquiere sentido la nueva acepción del término «arqueología».
Si actualmente la historia se centra en los documentos relacionados entre ellos
y lo que está fuera de ellos. Si la arqueología es una disciplina que se ha
centrado en tratar de hacer hablar a los monumentos y vestigios del pasado.
Entonces el análisis arqueológico responde a la descripción intrínseca de los
documentos: ha hacerlos hablar a partir y sin ir más allá de su discursividad.

<!-- Primera parte: capítulo X -->

<!-- Segunda parte: capítulo X -->

<!-- Tercera parte: capítulo I -->

\vskip 1em\noindent Durante la introducción, Foucault tomó las precauciones
necesarias para que el análisis arqueológico no fuera reducido a otra 
disciplina. La arqueología se desligó de manera total de la historia «global», 
se delineó su partida desde la historia «general» y se advirtió su similitud 
pero también su irreductibilidad al estructuralismo al señalar que en el 
análisis arqueológico no existe una oposición entre «estructura» e «historia». 
No obstante, en este punto de la obra aún no está del todo claro cómo la 
historia «general» funcionó como punto de partida.

El tercer apartado de *La arqueología del saber* se centrará en delinear las
diferencias por las cuales la historia «general» solo funciona como una 
plataforma para constituir el método arqueológico. La crítica a este tipo de
historia prestará especial atención a la historia de las ideas —o al menos a lo
que Foucault considera que es la historia de las ideas, ya que en ningún lugar
se encuentran referencias explícitas de los conceptos, métodos y discursos que
critica.

La explicitación del distanciamiento empieza admitiendo dos cuestiones:

1. La arqueología es complicada ya que de primera mano no se entiende el porqué 
   de otro análisis del lenguaje, ni de cómo toma distancia de los métodos 
   que la precedieron.

2. La historia de las ideas es difícil de caracterizar ya que no sigue un
   camino recto, sus métodos son de índole dispar, las delimitaciones
   discursivas no están bien delineadas y sus objetos de estudios tienden a
   ser difusos.

Sin embargo, en la historia de las ideas es posible observar dos papeles que
lleva a cabo. El primero es sobre cómo ha sido una historia de los anexos y los
márgenes. Una historia que no se caracteriza por analizar las formas consagradas
del saber, sino de esas tipologías que constantemente se categorizan como
opiniones, conocimientos imperfectos o supuestos filosóficos. El segundo es
la característica de elaborar historias transversales. Historias de los «juegos»
de cambios, sus intermediarios, sus oscuras continuidades o sus discontinuidades
a partir de diversas disciplinas —como las ciencias, la filosofía o la 
literatura— que atraviesan a la historia para reinterpretarla y encontrar en 
ella las ideas que le han servido de fondo.

Con estos elementos preliminares a contrapelo pueden mencionarse cuatro 
diferencias generales. Este nuevo método no busca interpretar o ser alegórico,
su asignación de verdades no pretende definir lo que está debajo de un
discurso, sino sobre cómo este es una práctica que obedece a ciertas reglas. La 
arqueología no es «doxología», sino análisis diferencial; es decir, en su 
estudio de las contradicciones no tiene la intención de llevar a cabo una lenta 
progresión hasta dar con una estabilidad, sino que se queda en el juego de 
reglas que es la condición de posibilidad de estas contradicciones. El análisis 
arqueológico toma como ajeno al sujeto como razón y principio de unidad: sus 
comparaciones no giran en torno a la obra sino a las reglas que están presentes 
al menos en una parte de la práctica discursiva de las obras. Por último, la 
metodología que se encuentra en desarrollo no trata de volver a los orígenes, 
sino que ubica las transformaciones sobre lo que se ha reescrito en lugar de ir 
al instante donde el discurso ocurre.

<!-- Tercera parte: capítulo II -->

\vskip 1em\noindent La historia de las ideas no solo asume dos papeles sino que
trata a los discursos desde un campo valorativo de dos polos. Por un lado los
discursos que no tienen antecedentes, haciéndolos relativamente escasos. Estos 
inducen al historiador a buscar una línea evolutiva en una historia que está 
enmarcada en los despertares de conciencia e invenciones. Por otro lado la masa 
de discursos valorados como triviales. El historiador se vale de ellos como 
fuente para erigir figuras globales en una historia que se presenta como mera 
acumulación del pasado.

Este quehacer que legitima a ciertas relaciones como características de un
conjunto documental a su vez desprende dos problemas metodológicos. La 
precesión —la búsqueda de antecedentes— no es suficiente ni una medida absoluta 
ni mucho menos un dato irreductible y primero. Más bien, en su indagación 
subordina ya los discursos que analiza porque los hace relativos al mismo 
sistema discursivo que erige para examinarlos. Este encadenamiento en aras de 
localizar los discursos originales también trae consigo el criterio de la 
semejanza. No obstante, la similitud no se reconoce de manera inmediata, sino
que requiere la creación de analogías para crear un efecto en el campo 
discursivo donde se sitúa.

En pocas palabras, la originalidad documental que rebusca la historia de las
ideas no es la aproximación más pertinente. Si bien la exigencia de originalidad 
y las tramas que esto implica pueden resultar muy entretenidas, solo tienen 
sentido en series muy definidas dentro de un quehacer historiográfico que 
precisamente se caracteriza por su ausencia de un objeto bien delimitado.

La arqueología se aleja de la oposición entre la originalidad y la trivialidad
para orientarse a la regularidad de los enunciados. ¿Qué hace patente la 
«regularidad»? Sin importar que los enunciados sean originales o imitativos,
no existe una diferencia de fondo entre uno y otro. Esto deriva que la 
regularidad no pierde su intensidad en las constantes repeticiones, haciendo
que el dominio discursivo esté continuamente activo. No hay historias de grandes
invenciones o de solo acumulaciones, sino historias que se dedican a mostrar
la cotidianidad de las prácticas discursivas.

Esto facilita visualizar dos direcciones en las que se dan paso estas
regularidades. Una es referente a la *homogeneidad enunciativa* que, siendo
distinta a la identidad lógica o a la analogía lingüística, permite observar
el surgimiento de una nueva práctica discursiva aunque no existan diferencias
en cuanto a la analogía o la equivalencia. La otra saca a colación las 
*jerarquías enunciativas* las cuales explicitan que las reglas de juego no están
presentes en un solo enunciado sino en una ramificación donde algunos son más
generales o independientes y otros más particulares o subordinados. En este
sentido es como el análisis arqueológico se separa del paradigma historiográfico
de las «épocas» y se vuelca sobre elementos más específicos de estas; a saber,
los «periodos enunciativos».

<!-- Tercera parte: capítulo III -->

\vskip 1em\noindent El espacio conflictivo entre la originalidad y la 
trivialidad, y su proceso de estabilización no es el único por el cual la 
historia de las ideas intenta un punto de cohesión. Las contradicciones también
se perfilan a una pacificación mediante una unidad oculta. Un primer problema
que salta es que este tipo de historiador debe suponer la existencia de dicha
unidad para así orientar su práctica al restablecimiento de semejante cohesión.

Existen varios caminos para dar con esta unidad. Su cumplimiento puede ser
a través de la determinación de principios lógicos de no contradicción, la
creación de hilos analógicos o simbólicos, la constitución de parentescos según 
los grados de explicitación de los discursos, o la puesta en un plano del 
individuo y su circunstancia o la sociedad y las culturas. Como se dé el caso, 
al final el investigador obtiene dos niveles de contradicciones: las 
residuales, que se resuelven en una unidad profunda del discurso, y las 
fundamentales, que dan cabida al mismo discurso. El quehacer historiográfico se 
resume a una actividad que se encarga de desaparecer o reaparecer las 
contradicciones: se convierten estas en principio de historicidad.

El análisis arqueológico niega esta postura y funda a las contradicciones como 
objetos por sí mismos, irreductibles a unidades. La contradicción, más que una 
función general que rige el discurso histórico, es tratada como tipos, niveles 
y funciones. Del tipo de contradicciones existentes, solo las *intrínsecas* son 
pertinentes para este método, ya que son las únicas que se despliegan en la 
misma formación discursiva, en cuyos diferentes niveles demuestran que no son 
puras y simples, sino que presentan una complejidad a modo de funciones que 
abren nuevas secuencias discursivas, las reorganizan o ponen en juego su propia 
existencia.

<!-- Tercera parte: capítulo IV -->

\vskip 1em\noindent La negación por parte de la arqueología de disolver
contradicciones hace que la comparación de los hechos se lleve a cabo de una
manera diferente. Al no haber unificación, existe una individualización de los
discursos que no pretende analizarse como se examina la estructura interna de
una teoría. Más bien, existen tres pautas generales durante esta comparación.

En un primer momento, esta individualización hace que la comparación sea 
limitada de manera deliberada: no describe totalidades, como podría ser una
*Weltanschauung*. La limitación no implica una simplificación del análisis, sino 
su singularización al punto de desconocer la cantidad de relaciones efectuadas,
al unísono que niega la posibilidad de categorizar a las prácticas discursivas 
como ciencia, racionalidad, mentalidad o cultura. Esta apertura no se desborda 
hasta hacerlas inmanejables porque los discursos presentan reglas de formación 
que señalan cinco tareas: la posibilidad de formación a través de reglas 
análogas, la medida en que las reglas son aplicables, la conveniencia de que 
diferentes conceptos puedan ser análogos, la manera sobre cómo una noción puede 
englobar dos elementos distintos y la coyuntura que permite el establecimiento 
de positividades. Esto respectivamente se conoce como *isomorfismos*, 
*modelos*, *isotopías*, *des-fases* y *correlaciones* arqueológicos. Por último,
esto pone de manifiesto que existen relaciones con dominios no discursivos
—como las instituciones, los acontecimientos políticos o los procesos 
económicos— donde el análisis no se vuelca a estas exterioridades, sino a sus 
modos específicos de articulación con la práctica discursiva.

La comparación de los hechos permite al análisis arqueológico descubrir el
campo de dominio y el funcionamiento de las prácticas discursivas, las cuales 
están insertadas en historias «generales». Sale a la luz que la historia da 
hincapié a los discursos en su historicidad específica.

<!-- Tercera parte: capítulo V -->

\vskip 1em\noindent Cuando parecía que la crítica ya se alejaba de la historia
de las ideas, se regresa a esta para resaltar una característica que al parecer 
carece el análisis arqueológico: en este método no se observa una cronología y, 
por ende, cambios y transformaciones, sino reglas generales asincrónicas. 
Foucault pide al lector que se vean las cosas más de cerca, específicamente en 
dos aspectos.

En primer lugar, las formas discursivas dan muestra de su temporalidad a través
de sus relaciones. Estas ayudan a ver las posibilidades de correlaciones con 
nuevos enunciados o con acontecimientos exteriores en una red que no se da de 
manera simultánea, mucho menos de modo lógico o lineal, sino como 
transformaciones *sucesivas* que superponen los discursos en lugar de 
encadenarlos. Esto faculta, en un segundo aspecto, que a la arqueología se le
facilite hablar de rupturas, cuyo proceso de diferenciación —que no ha sido
inventado, porque ya está presente en la práctica discursiva— opera de cuatro
modos.

Se inicia con la distinción de los planos de los enunciados, de la aparición de 
objetos, de la derivación de nuevas reglas de formación y de sustituciones
discursivas. Una vez diferenciados los planos, es posible definir las múltiples
transformaciones como las que acontecen en los diferentes elementos de un 
sistema o en las relaciones de este, con las reglas de formación o entre 
distintas positividades. Lo relevante de estas transformaciones es que le quita 
el carácter abstracto al cambio como en sus formas de «movimiento», «flujo» o 
«evolución»; es decir, destruye leyes universales de la transformación para 
volver al cambio en algo analizable. Este queda acotado como principio de 
multiplicidad y dispersión de fenómenos de continuidad, aparición, retorno o 
repetición de elementos de las prácticas discursivas. Pero también a la
descripción de las discontinuidades, donde las rupturas son la desarticulación
de la sincronía sin que se postule como un límite, ya que es la manera en como 
acontece el discurso.

La intención aquí es hacer patente que el método arqueológico no niega ni ignora 
las transformaciones y los cambios en los acontecimientos que están presentes en
el discurso histórico. En su lugar, pretende evidenciar que en la medida que se
tilda a la arqueología de estaticidad, es una señal de qué tanto en la noción de
«cambio» y de «transformación» perviven algunos de los supuestos de las 
filosofías de la historia donde estas concepciones son indisociables de la
historia como «progresión» lineal desde un origen y hacia algún  *telos* desde 
y para el sujeto.

<!-- Tercera parte: capítulo VI -->

\vskip 1em\noindent El último capítulo de esta sección es el más extenso por el
siguiente motivo: aquí es donde se regresa a relacionar el análisis arqueológico
con las ciencias, filosofías, literaturas y textos políticos. Este inicia con
una simple pregunta: ¿por qué la arqueología se ha empecinado en disciplinas
cuyas cientificidades son dudosas?

La respuesta inmediata es sencilla. El análisis arqueológico no se fija en
disciplinas: las positividades que analiza ignora gran parte del trabajo 
elaborado por estas disciplinas, haciendo que las formaciones discursivas no
sean ciencias y, por ello, son inidentificables.

La asimilación no es posible porque las positividades no caracterizan formas
*a priori* del conocimiento o de racionalidad, ni estados de los conocimientos
en un momento dado del tiempo; intenta mostrar las reglas por las que se 
pretenden construir desde proposiciones coherentes hasta teorías. Estas reglas
son antecedentes que, *si se da el caso*, constituyen discursos científicos. En
otros términos, las positividades se fijan en el saber como aquellas condiciones
de posibilidad de una práctica discursiva, su utilización y su apropiación; un 
dominio de sus objetos que *tal vez* adquieran un estatuto de cientificidad; un 
lugar que funge de espacio en donde el sujeto puede hablar de los objetos de un 
discurso y de campo donde los conceptos se definen, se aplican y se transforman.

El saber queda como el fondo de las ciencias, por lo que no depende de ellas.
Aquí ya se puede entender el título del libro: el análisis arqueológico se
perfila al saber porque este proviene de una práctica discursiva independiente 
o que yace de base al discurso científico *o no*: la posibilidad, el dominio y 
el lugar para un discurso racional *o no*.

Con esto, por fin puede indicarse que el eje de la arqueología radica entre la
práctica discursiva, el saber y, por último, la ciencia. Esto la distingue de
la historia de las ideas ya que este modo de proceder se centra sobre el eje de
la conciencia, el conocimiento y la ciencia, donde el sujeto y su actividad se
evidencian como el nexo. Mientras tanto, en el eje del análisis arqueológico el 
sujeto deja de ser titular y es puesto como situado y dependiente.

Esta diferencia de ejes provocan la necesidad de distinguir entre 
*dominios científicos* y *territorios arqueológicos*. En el dominio las 
proposiciones cumplen ciertas reglas de constitución donde son excluidas todas
las verdades que no nazcan de esta sistematicidad. Por otro lado, los 
territorios atraviesan discursos de diversa índole donde el saber se manifiesta
como demostración pero también como ficción, relato, reglamento institucional
o decisión política.

El hecho de que el discurso científico y sus reglas eviten absorber todas las
prácticas discursivas por las que surge, hace que su carácter ideológico no
se localice en su misma sistematicidad. La ideología yace en el saber y se 
cuela como práctica discursiva en funcionamiento con otras prácticas, por lo 
que: no es exclusiva de la cientificidad, las lagunas en el discurso científico 
permiten señalar su función ideológica, su depuración no garantiza su 
erradicación —no es un elemento que pueda eliminarse, sino que forma parte de 
la misma dinámica del discurso—, así como su análisis no implica develar los 
supuestos filosóficos de la ciencia —no va a los fundamentos, sino que la pone 
en cuestión.

La carencia de una linealidad histórica hace que la historia de las ciencias
se rija más bien en especies de umbrales. Uno es el de la positividad que
acontece cuando una práctica discursiva al fin se individualiza y adquiere su
autonomía; umbral solo perceptible por el método que se fijará en las prácticas
discursivas: el método arqueológico. El epistemológico que se da cuando se acota 
el conjunto de enunciados para hacer valer unas normas dominantes sobre el 
saber; el análisis histórico buscará evidenciar aquí cómo se instaura una 
ciencia. El de cientificidad donde las figuras arqueológicas empiezan a obedecer 
a *leyes* de construcción de proposiciones; el historiador se volcará a la 
descripción del proceso de depuración de conceptos para mostrar cómo la 
oposición entre la verdad y el error dio fruto a la cientificidad del concepto. 
Por último, el umbral de formalización donde el discurso científico define sus 
axiomas, sus elementos, las estructuras proposicionales que le son legítimas y 
las transformaciones que estará dispuesto a aceptar; la historia se cristalizará 
como un pasado sumergido en la ingenuidad hasta que de manera progresiva se ha
llegado a la formalización de la ciencia.

Cabe resaltar que la cronología de estos umbrales no es en un solo sentido,
homogénea o de modo evolutivo. Ningún umbral es en sí el punto de partida y
el otro el de llegada, sino que es posible pasar por alto alguno de ellos.

Además, existe otro tipo de historia de la ciencias que no se sitúa en ningún
umbral, sino entre sus relaciones y desde donde es posible percibir un conjunto 
de nexos que es la *episteme*. Esta no es una visión del mundo, estadio de la 
razón, forma de conocimiento o unidad aglutinada en un sujeto, espíritu o época. 
La episteme no es una derivación de los supuestos en el quehacer historiográfico 
que se han venido criticando. En su lugar, es un recorrido de relaciones 
indefinidas, un modo de aprehender las limitaciones de una práctica discursiva, 
la posibilidad de las ciencias, lo que está presente en los procesos de la 
práctica histórica, y lo que pone en juego y cuestiona el hecho de que exista 
eso que llamamos ciencia.

Pese a que el análisis arqueológico implícito en *Historia de la locura*, 
*El nacimiento de la clínica* y *Las palabras  y las cosas* se enfocó a
la episteme, el hecho de que la arqueología no tenga como centro al conocimiento
sino al saber implica que existen otras posibilidades de análisis. Los ejemplos
pueden ser la sexualidad como campo de enunciaciones posibles; la creación
artística como práctica discursiva que toma cuerpo en su técnica y sus efectos,
o el saber político no como teorización de una práctica o la aplicación de la
teoría, tampoco como nexo con alguna clase de conciencia —incluida la conciencia
revolucionaria—, sino como espacio de diferentes prácticas en las que encuentra 
su especificidad.

<!-- Cierre contextualizado -->

\vskip 1em\noindent El año 1969 fue de mucha agitación para Foucault. Los 
siguientes años no serían muy distintos. En 1970 es nombrado profesor en el
Collège de France a la par que empieza a dar una serie de ponencias en otras
partes del mundo (Castro, 2014). A partir de ahora Foucault continuará —y 
divergirá— con otras posibilidades arqueológicas: la historia de la sexualidad y
los análisis sobre el ejercicio del poder.

<!-- Comentarios -->

\vskip 1em\noindent Para terminar, quisiera plantear dos cuestiones que se
relacionan a lo que se ha visto a lo largo del curso. La primera es una pregunta
abierta: ¿qué relación o qué impacto puede tener el análisis arqueológico en
la actualidad política, considerando aquella posibilidad de hablar de «praxis»
política sin necesidad de recurrir a nociones como «teoría», «práctica» o
«conciencia» —elementos usados por Sánchez Vázquez para darle significado y
sentido a este tipo acción?

Lo segundo es una hipótesis a discutir. A lo largo de *La arqueología del saber* 
se retoman las relaciones del conocimiento científico con su fondo común a 
discursos no científicos. Considerando las diferencias entre el dominio 
científico y el territorio arqueológico, ¿en qué lugar sería posible situar al 
discurso filosófico, el filosofar y la filosofía? Es decir, al parecer la 
práctica filosófica presente en su discurso, la acción implícita en el filosofar 
y esa disciplina que llamamos filosofía queda situada en el territorio
arqueológico. De ser así, las implicaciones podrían ser:

* El discurso filosófico no se distingue por su sistematicidad, sino por
  discontinuidades.
* El filosofar no es tanto una actividad rigurosa, sino una especie de «juego».
* La filosofía más que vérselas con conocimientos y verdades, se constituye
  como saberes y tentativas.

Foucault emplea mucho la palabra «juego» cuando está describiendo elementos
presentes en el territorio arqueológico. Si efectivamente la filosofía yace en
este plano, ¿qué tan válido es pensar que la enseñanza de la filosofía debería
centrarse en «jugar con las ideas», en orientarse a lo que no son propiamente 
verdades, a las contradicciones, a lo que carece de unidad, en fin, a la 
discontinuidad —estrategia pedagógica quizá muy distinta a las planteadas por 
Gaos cuando habla del contenido de la enseñanza como lecciones, conferencias, 
comentarios al texto, ejercicios o diálogos?

### Fuentes

1. Castro, Edgardo (2014). *Introducción a Foucault*, Buenos Aires, Siglo XXI. 
   En línea: [epublibre.org/libro/detalle/24367](https://epublibre.org/libro/detalle/24367).
2. Michel, Foucault (1984). *La arqueología del saber*, México, Siglo XXI. En 
   línea: [epublibre.org/libro/detalle/23041](https://epublibre.org/libro/detalle/23041).


[^1]: Disponible en: [alturl.com/6wrsx](http://alturl.com/6wrsx).


<!--
	\vskip 1em\noindent 
	pandoc foucault_michel-la_arqueologia_del_saber-completo.md --latex-engine=xelatex --template=0-template.latex -V lang:es -V documentclass:book -V papersize:a5 -V classoption:oneside -V geometry:margin=1in -V indent:true -V title:"El análisis arqueológico" -o foucault_michel-la_arqueologia_del_saber-completo.pdf
-->
