# *Filosofía de la praxis*, Adolfo Sánchez Vázquez

## Introducción

«Praxis» como acción.
  - Sinónimo de modod general con «práctica».
    - Práctica es más para el lenguaje común y literario, estrechamente
      utilitario.
    - Praxis es más filosófico.
  - No en el sentido griego como acción con un fin en sí misma y sin producir
    algo ajeno.
    - P. ej. la acción moral.
    - Poiésis es la acción que produce algo, como el quehacer del artesano.

### La conciencia ordinaria de la praxis

La praxis ocupa un lugar central en la filosofía, si esta se entiende como
transformación del mundo.
=> Marxismo.
  - La conciencia de la praxis tiene antecedentes y no está acabada con Marx.
    - Lejos de conciencia idealista u ordinaria.
  => Superación.

El concepto de la praxis es central en el marxismo:
  - vs conciencia ordinaria de la praxis: la práctica cotidiana la oculta.
  - vs conciencia idealista: la mistifica, aunque supera a la conciencia
    ordinaria.
=> Superación para la conciencia verdadera que una el pensamiento y la acción.

La actitud cotidiana coexiste con la actitud filosófica.
  - Partir de ella para una concepción filosófica de la praxis.
  - Cree estar en una relación directa sin necesidad de desgarrar el telón:
    ateórica.
  - La teoría solo para necesidades prácticas.
=> vs: no hay encuentro desnudo sino ideológicamente determinado por su
   situación sociohistórica.
    - La praxis ya está determinada por estos principios frutos de un
      bagaje teórico.
    - Ve a la acción como carente de transformación humana.
    - No hay conciencia del lazo entre la conciencia y su objeto.
    - Incapaz de una teoría de la praxis.
  => Incapaz de una verdadera praxis revolucionaria.
    - Necesidad de abandono para abrir la posibilidad de creación 
      transformadora.

El hombre común se tiene como el hombre práctico verdadero.
  - Las cosas existen en sí y por sí para la satisfacción de necesidades.
    - Olvida que existen por y para el hombre.
    - Objetivismo que separa al objeto del sujeto.
  - Lo práctico se reduce a lo práctico-utilitario.
    - Impráctico = carencia de utilidad directa.
    - Coincide con el enfoque de la producción capitalista.
    => Deforma a la conciencia política a una «carrera».
      - Despolitización de la conciencia que conviene a la clase dominante.
        => Ideología burguesa.
    - Las artes, la política, la teoría y la acción revolucionaria se vuelven
      «imprácticas».
    - Se percibe como autosuficiente, por lo que se presenta como entendible de
      suyo: habla por sí mismo.
  - Separación de la teoría y la práctica.
    - Hay idea de praxis pero de forma degradada.
    - Praxis como *utilidad*, *individualidad* y *autosuficiencia* (ateórica).
    - Olvida que con sus actos está escribiendo la historia humana.
=> No puede ser superada por la conciencia ordinaria.
  - Necesidad de una conciencia que la capte:
    - En su totalidad histórica y social.
    - En su integración de formas específicas.
    - En sus manifestaciones particulares de las actividades de los individuos.
  - No se alcanza casualmente, sino históricamente.
    - Maduración en la historia de las ideas.

### Bosquejo histórico…

En sus orígenes, la filosofía ha rechazadoel mundo práctico, por ver en ella
lo mismo que percibe la conciencia ordinaria: su carácter práctico-utilitario.

El trabajo era indigno para el hombre libre.
  - Ensalzar la actividad contemplativa.

La *polis* como la expresión más alta del proceso de transformación.
  - La transformación de las cosas tenía un carácter secundario.
    - No se ve su capacidad de transformación humana.
    - Se percibe como esclavización.
=> Aislamiento de la teoría.
  - Principal cúspide en Platón y en Aristóteles.
    - Platón: la vida es contemplación.
    - Aristóteles: la actividad práctica manual no tiene significación humana.

La praxis material hace al hombre esclavo de la materia.
  - El ocio para ser hombres libres.

<!-- Más que esclavitud es la intuición de que la relación con las cosas culmina
en una determinación de lo que se es, pero sin percibir que la relación con
las cosas puede ocultarse, mas no eliminarse -->

Platón y Aristóteles han admitido la legitimidad de la praxis política, pero
sin renunciar a la vida teórica.
  - Platón:
    - Descanso de la práctica en la teoría.
    - El filósofo no se halla supeditado a la *polis*.
    - Unidad es más bien disolución de la práctica en la teoría.
  - Aristóteles:
    - No hay unicidad, sino subsistencia en distintos planos.
    - La praxis es parte del intelecto práctico.
=> Concepción del ser humano como ser racional o teórico.
  - El trabajo es visto en función del producto.
    - Cualidad y valor de uso, en lugar de cantidad y valor de cambio.
  => vs (Hesíodo): el trabajo tiene un valor de redención y de creación de
     riquezas.
     vs (Antifon): el trabajo como penalidad en un contexto de igual naturaleza
     humana.
     vs (Pŕodico y Antístenes).

La conciencia de la praxis sufre un cambio en el Renacimiento.
  - El sujeto se hace activo y creador del mundo.
  - El hombre como ente de razón y de voluntad.
    - Razón => Comprensión.
    - Voluntad => Transformación.
=> Alineación con los intereses de la burguesía.
  - El conocimiento científico no es ya válido por sí mismo, sino al servicio
    de la producción capitalista.
    - Fortalece su modo de producción.

La actividad productiva pasa a ser parte del hombre libre, pero con un carácter
limitado.
  - Condición necesaria para la libertad.
  - Subsiste la división de la teoría y de la práctica.
  - Como acercamiento a lo humano que es la contemplación espiritual.
=> vs (Sacchi y Palmieri): la especulación aísla y se aleja del bien público.

La actividad creadora pasa a ser actividad privilegiada.
  - Implica una falta de libertad de los desafortunados.

En Maquiavelo la teoría pasa a esar a servicio de la práctica.
  - La teoría para abrir paso a los intereses de la naciente burguesía.

El dominio de la naturaleza mediante la producción, la ciencia y la técnica
se convierte en una cuestión central.
  - Necesidades y determinaciones sociales.
  - La práctica como actividad experimental científica.
  - Guiada por la teoría.
  - Transformación utilitaria en lugar de una que opera sobre el hombre.
=> vs: Rousseau indica que ha transformado negativamente al hombre.
  - Anticipación de ideas de Marx.

### Hacia la reivindicación…

Los economistas clásicos han hecho ver al trabajo como fuente de riqueza y de
todo valor.
  - Para Marx y Engels se percibe limitada.
    - La praxis queda reducida a un concepto económico.
    - El trabajo no es considerado en su forma histórica concreta (trabajo
      enajenado o asalariado).
  => Superación cuando se conceptualice al hombre como ser activo y creador.
  - Hegel como paso decisivo.
  - Feuerbach como paso del Espíritu a una medida humana.
  => Marx y Engels forjaron una concepción del hombre como ser creador.
    - La producción e historia forman una unidad indisoluble.

La praxis pasa a ser algo central en Marx.
  - Permite abordar el problema de la historia, la sociedad y el ser mismo.

### Convergencias y divergencias

Los marxistas remiten a las *Tesis sobre Feuerbach* las ideas sobre la praxis.
  - Existe el problema en el marxismo sobre la significación de la praxis,
    de lo que depende la percepción y del marxismo como:
    a. Una filosofía más como interpretación del mundo.
    b. Una filosofía de la acción transformadora.

### La praxis como categoría filosófica…

La praxis más cercana a la autoemancipación del hombre es la que está vinculada
con la creatividad.
  - Creación como producción de una nueva realidad que no existe por sí.
  - Determinar el papel de la violencia, que se entiende como negación de la
    creación.
=> Necesidad de esclarecimiento.
  - Gramci como punto sugestivo.
    - Acentúa el factor subjetivo en la historia real.

## ¿Qué es la praxis?

### Actividad y praxis

Toda praxis es actividad, pero no toda actividad es praxis.
  - La praxis es una forma de actividad específica.

Actividad: acto por el cualel sujeto modifica una materia prima dada.
  - Una concepción muy general.
  - Opuesta a la pasividad.
  - En la esfera de la efectividad.
  - Estructurados en un todo.
=> Solo es actividad humana si contempla:
  - Un fin.
  - Un resultado efectivo.
=> Una determinación que viene del futuro.

### La adecuación de fines

La actividad específicamente humana requiere la intervención de la conciencia.
  - El resultado existe dos veces:
    1. Resultado ideal (primero temporalmente).
    2. Producto real.
=> No importa su distancia, sino la adecuación, aunque sea imperfecta.
  - Implica intencionalidad.
  - Presente en la actividad individual y social.
  - Conforme a fines.

El fin es cierta actitud ante la realidad.
  - No es armonía, sino negación ideal y reconfiguración real.
    - No tendría sentido proponerse un fin ya alcanzado.
  - Prefigura idealmente.
  - Negación de la realidad efectiva y afirmación de una que no existe todavía.
  - Implica la conciencia pero también una relación de exterioridad.
  - Media entre el pensamiento y la acción.

La actividad humana es cognoscitiva y teleológica,partes de la actividad de
la conciencia.
  - Cognoscitiva: referida a una realidad actual a la que pretende dar razón.
    - No implica la acción efectiva.
  - Teleológica: referida a la concreción de una realidad futura.
    - Exige una acción efectiva.
=> No rebasa lo que está más allá de la conciencia; es decir, no se objetiviza
   o materializa.

### La conciencia práctica

Su carácter distintivo es su resultado objetivo.
  - Actúa sobre una materia independiente.
  - Exige actos físicos.
  - El producto subsiste con independencia.
  - Su objeto es la naturaleza, la sociedad o los hombres reales.
  - Genera una nueva realidad que existe por y para el hombre.

### Formas de praxis

Los objetos pueden ser:
  a. Lo dado naturalmente
  b. Los productos de la praxis anterior.
  c. Lo humano mismo.

La praxis productiva es la que el hombre establece con la naturaleza.
  - Vencimiento de la resistencia material para la creación de objetos útiles.
  - Requiere de relaciones de producción.
  - Impresión de la marca de los fines en la materia.
  - Humanización como expresión de la relación del hombre y la naturaleza, y
    de las relaciones sociales.
    - Una paulatina mediación del instrumento.
  - Producción de un mundo humanizado.
    - Satisfacción de necesidades humanas.
    - Transformación de sí mismo.

La praxis artística es la impresión no ya práctica-utilitaria, sino de necesidad
de expresión y de comunicación.
  - Creación de otra realidad.
  - Esencial para el hombre.
  - No es mera producción materialni pura producción espiritual.

La praxis experimental satisface las necesidades de investigación teórica y
de comprobación de hipótesis.
  - Modificación de las condiciones por las que opera un fenómeno.
  - Reproducción del fenómeno para su estudio en un medio artificial, que
    elimina perturbaciones.
  - El fin inmediato es teórico.
  - El experimento no está al servicio directo sino quizá sus resultados.

La praxis política es la por la cual el hombre actúa sobre sí mismo.
  - Transformación como ser social.
  - Su objeto no es un individuo, por lo que puede llamarse praxis social.
  - En torno a la actividad del Estado en tres ejes:
    1. Lucha de clases.
    2. Lucha ideológica.
    3. Conquista del Estado.
  - El poder es un instrumento vital.
  - Presupone la participación de amplios sectores de forma consciente,
    organizada y dirigida.
    - Justificación de la existencia de los partidos.
  - Su forma más alta es la praxis revolucionaria.
    - Cambio radical de las bases.
    - El agente principal es el proletariado.

### La actividad teorética

Solo existe por y en relación con la práctica.
  - No es po sí una forma de praxis.
    - No transforma la realidad.
    - No tiene lado material.
=> Por ello no es legítimo las «praxis teóricas».

La distinción de la práctica es por sus objetos, fines, medios y resultados.
  - Sus objetos son ideales.
  - Sus fines son las transformaciones ideales.
  - Sus medios son operaciones mentales.
  - Sus resultados son ideas sobre el mundo.

La praxis teórica va en contradicción a la crítica de Marx.
  - No se admite la teoría como praxis sino como elementos distintos.
  - La teoría no transforma la realidad.
  - Solo posible si a la praxis se le quita lo específico y se le entiende como
    actividad en general.

### Filosofía y praxis

Desde un punto de vista histórico las filosofías pueden dividirse en:
  1. Filosofías que dan razón de lo existente.
  2. Filosofías que transforman el mundo.
    - Tiene carácter científico.
    - Se ve al servicio de la praxis; es decir, no es praxis.

## Unidad de la teoría y de la práctica

Existe oposición entre lo teórico y lo práctico:
  - Lo teórico transforma conciencia de los hechos.
  - Lo práctico es acción efectiva.

### El punto de vista del sentido común

La oposición es relativa, es más bien una diferencia, es falso que:
  1. La práctica se desligue de la teoría.
  2. La teoría niegue vincularse a la práctica.
=> El primer modo sucede en la conciencia ordinaria.
  - Lo práctico se opone a lo teórico, es innecesaria y nociva.
  - Lo práctico se envuelve entre prejuicios, verdades aquilosadas y
    supersticiones.

En la historia de la filosofía se observa una continuidad en la oposición hecha
por el sentido común.
  - Lo verdadero = lo útil: pragmatismo.
  - Lo práctico como acción subjetiva del individuo para satisfacer sus
    intereses.
  - El criterio de verdad es el éxito.

### La práctica como fundamento de la teoría

La práctica es el fundamento de la teoría porque determina su horizonte de
desarrollo.
  - Presente desde las sociedades esclavistas.

### La ciencia y la producción

Las ciencias se benefician por las exigencias de la producción.
  - Su nacimiento es tardío, hasta Galileo.
  - Las ciencias más ligadas a la condición técnica son las que progresan más
    rápido.
  - El lazo es tal, que no se comprendería el incremento de las fuerzas
    productivas sin el progreso científico.

### Unidad de la teoría y práctica revolucionaria

La práctica sirve como criterio de verdad de la teoría marxista que se había
estado exponiendo.
  - Fuente de enriquecimiento.
  - Noción de necesidad de destrucción del aparato estatal.
  - Análisis de la práctica y su fundación teórica.
    - Fundamento para ir del capitalismo al socialismo.
    - El proletariado como agente de esta transformación.
  - La teoría no se desarrolla en aras de la Teoría sino de la práctica.

### La práctica como fin de la teoría

Otra relación es: teoría elaborada de una práctica que es inexistente.
  - La práctica es fin que determina la teoría.
  - Exige conciencia de la necesidad práctica.
  - No es una relación directa o inmediata.
  - La teoría responde a necesidades prácticas.
=> Supremacía de la práctica pero como algo que presupone la teoría, no
   en contraposición.

### Praxis y comprensión de la praxis

La teoría tiene una autonomía relativa.
  - Su conexión con la praxis no es directa, sino a través de la comprensión
    de la praxis.
    - Por ello no son disolubles.

### La praxis como criterio de verdad

Que la práctica no hable directamente ella misma no implica que no sea un
criterio de verdad.
  - El criterio reside en la comprobación de los procedimientos teóricos.

### Autonomía relativa de la teoría

La autonomía de la teoría permite que pueda adelantarse a esta.
  - El adelanto es ideal e influyente en la práctica.
  - Supone una mutua dependencia.

### La práctica como actividad…

La praxis es actividad teórica-práctica.
  - La actividad práctica humana reside en integrar lo subjetivo en un proceso
    objetivo, exterior.
    - Necesidad de un objeto exterior.
    - No es platonismo de la dualidad original/copia.

## Praxis creadora y praxis reiterativa

### Nieveles de la praxis

La praxis puede clasificarse según el grado de penetración de la conciencia y
el grado de humanización:
  - No elimina vínculos mutuos.
  - Contexto de una praxis total.

Primera clasificación:
  1. Praxis reiterativa: conforme a una ley trazada para la producción de
     múltiples productos.
  2. Praxis innovadora: creasin adaptarse a una ley y desemboca en un producto
     nuevo y único.

### La praxis creadora

La praxis creadora hace frente a nuevas necesidades.
  - Las soluciones siempre pasan a ser obsoletas.
  - Las repeticiones se justifican mientras la validez se mantenga.
  - Se hace un mundo más humano y se hace a sí mismo.
  - Se trata de una unidad entre lo subjetivo y lo objetivo, y lo interior y
    lo exterior.

No es una serie de actos, sino un solo acto abierto y activo durante toda la
praxis.
  - No da un producto acabado, sino un fin abierto y un proyecto dinámico.
  - Acontece una transformación ideal en respuesta a las exigencias externas.
  - No es impresión de una forma preexistente y acabada a la materia.
  - El objeto ideal y el material son caras de una misma moneda.
    - Preconfiguración ideal.
    - Resultado valedero en lo material.
  - Carga de incertidumbre por la distancia cada vez mayor entre lo ideal y lo
    material a lo largo del proceso.
    - Pasa de presidirlo a ser leyque rige la totalidad del proceso.
      - Solo distinguible a posteriori, lo que hace único e irrepetible.

En conclusión, la praxis creadora:
  1. Tiene unidad indisoluble.
  2. Contempla imprevisibilidad del proceso y del resultado.
  3. Implica unicidad e irrepetibilidad del producto.

### La revolución como praxis creadora

La revolución crea un régimen social nuevo para responder a las necesidades.
  - El proyecto se modifica desde su concepción hasta su realización efectiva.
    - Imprevisibilidad en el proceso.
  - Cada revolución es única e irrepetible.

### La creación artística

La creación artística es la expresión más clara de la praxis creadora.
  - Hay unidad porque la forma nace del proceso y no como algo preexistente a la
    materia, que culmina en la obra.
    - No hay distinción entre gestación interna y ejecución externa.
  - Es un proceso imprevisible, ya que el proyecto solo se determina a partir
    de su realización.
  - Da como resultado un producto único: la obra de arte.

### La praxis reiterativa o imitativa

Es de nivel inferior a la praxis creadora.
  - No tiene los tres rasgos o su manifestación es débil.
    - No hay unidad, sino plan preexistente y acabado.
    - Se angosta el plan de lo imprevisible ya que no se inventa un medio de
      hacer, sino que ya es conocido.
    - No da productos únicos, sino que es un proceso de multiplicación
      cuantitativa del cambio cualitativo que hizo la praxis creadora.
  => Sus cualidades:
    - Positiva: extiende lo ya creado.
    - Negativa: puede cerrar el paso a la creación.
      - Peligro en la praxis revolucionaria y en la praxis artística.

### La praxis burocratizada

Un ejemplo de praxis reiterativaestá en la praxis burocratizada.
  - Es exteriorización o formalización de la práctica.
  - Puede repetirse hasta el infinito.
  - Una forma de praxis degradada.
  - Lo formal es implantación de lo ideal sobre lo real, es lo irreal.
  - Fenómeno del Estado divorciado del pueblo: explotación.
  - Impregna la relación entre los hombres.
  - No es propia del Estado, sino de cualquier organismo que necesita un cuerpo
    especial de funcionarios.

### La praxis reiterativa en el trabajo humano

También hay efectos negativos en la praxis productiva.
  - Por ejemplo, el trabajo en cadena en las sociedades capitalistas.
    - Exige la producción masiva, no apto para la producción de artículos únicos
      del trabajo artesanal.
    - Rompe con la unidad del proceso para el obrero, lo que indica la división
      del hombre mismo.
    - El hombre como apéndice de una máquina.
  - Se convierte en actividad parcelaria, unilateral y monótona.
  - Simplificación del trabajo para mayor previsibilidad.
  - Adscripción en una sola actividad es idiotismo profesional.
    - Atadura de por vida a una actividad fragmentaria.
=> Su superación por:
  - Nuevas condiciones sociales.
  - Nuevas técnicas.                                                           <

### Grandeza y decadencia de la mano

La mano es un órgano y producto del trabajo.
  - Su superioridad reside en la vinculación con la conciencia.
  - Punto de partida para dominar las cosas.
  - Muestra un modo sensible y concreto de las relaciones humanas.
  - Capaz de testimoniar los sentimientos más opuestos.
=> Humaniza y se consuma como hombre.

En la praxis repetitiva acontece una separación de la mano y la conciencia.
  - Para llegar a una praxis creadora se requieren nuevas condiciones del 
    trabajo.
    - No mediante una regresión al trabajo artesanal, ya que ha sido superado.
    - Sí mediante el reconocimiento de la humanidad del obrero.
=> Su solución implica la disminución del ritmo del trabajo.
  - La automatización eleva de nuevo el papel de la conciencia, al exigir el
    papel de la conciencia.

### La praxis imitativa en el arte

El arte tampoco escapa de la praxis imitativa.
  - Por ejemplo, cuando la creación se conforma a un canon ya establecido.
    - Se rige por una ley ya establecida o intenta imitar un proceso artístico.

## Praxis espontánea y praxis reflexiva

### La conciencia en el proceso práctico

No puede excluirse la conciencia de la actividad, queda a lo sumo minimizada.

### Conciencia práctica y conciencia de la praxis

Conciencia práctica: conciencia actuante en unidad con la realización de sus
fines.
  - Se eleva en la praxis creadora y disminuye en la praxis reiterativa.
  - Solo aplicable en la materialización.

Conciencia de la praxis: conciencia que se vuelve a sí misma y sobre la
actividad material en que se plasma.
  - Conciencia de la praxis = autoconciencia práctica.

Ambas muestran a la conciencia en su relación con el proceso práctico.
  - No se confunden.
  - No están separadas.

### Dos nuevos niveles de la praxis

Dos niveles según el grado de autoconciencia:
  1. Praxis espontánea.
  2. Praxis reflexiva.
  - No asimilable unívocamente con la praxis creativa o reiterativa.
  - Praxis reflexiva ≠ praxis inconsciente.

### Lo espontáneo y lo reflexivo en la praxis revolucionaria

Un problema en el movimiento revolucionario: ¿qué tanto ha de manifestarse la
conciencia?
  - No es válida la carga en uno de los niveles.
  - Se requiere de una conciencia que en la acción integre la misión histórica
    del proletariado.

### La misión histórica del proletariado en nuestros días

La misión histórica del proletariado:
  - No es mesianismo.
  - Es la transformación radical de la sociedad como necesidad y posibilidad.
=> No es fijada inexorablemente, sino que está vinculada a la clase que más
   vinculada está con la producción.
  - Ninguna reforma puede abolir su condición.

### El marxismo como filosofía del proletariado

El proletariado es una clase revolucionaria o no es nada.
  - El marxismo como teoría que ofrece al proletariado la conciencia de su
    situación.
    - Da la posibilidad real de negarse a sí mismo como clase y abolir su
      situación.

### El marxismo como ciencia y como ideología

La conciencia que pretende interpretar la realidad es una conciencia interesada.
  - Tiene intereses de clase.
=> El marxismo expresa los intereses del proletariado.
  - Este lo dota de un carácter ideológico.
  - Pero también es una ciencia al dar conocimiento verdadero.
  => No pueden disociarse.
    - Ideología sobre la base de la comprensión científica.

### El intelectual y el proletariado

El patrimonio culturalno está disponible por igual.
  - Relación entre la dominación material y la espiritual.
    - El burgués con el acceso cultural es quien puede crear la base teórica.
  => ¿Por qué lo haría?
  - Presión de la propia realidad.
  - La intelectualidad no constituye una clase social de por sí.
    - La vinculación con la burguesía no es necesaria, pero tampoco implica
      posibilidad de «pureza».

## Praxis, razón e historia

### Praxis intencional

Praxis intencional: intervención de la conciencia como proceso de realización de
una intención nada.

### Intención y resultado

Lo determinante de la práctica es su producto.
  - Lo subjetivo importa como intención hecha objeto.

Dos tipos de problemas en la praxis intencional:
  1. Grado de presencia de la intención del sujeto en el objeto.
  2. La relación que tiene la intencionalidad con la valoración del resultado.
=> Respuesta común: el producto es la intención ya realizada.
  - Indisolubilidad de la intención y el producto.

### La praxis intencional en el arte

La obra de arte no es intención creadora, ya que el producto no es igual a la
intención.
  - Vale por su objeto.
  - La inadecuación solo se entiende a partir de la intención originaria no
    realizada.

### La praxis intencional en la vida social

La praxis social es otro tipo de praxis intencional.
  - Produce una nueva realidad.
    - La forma más elevada es la praxis revolucionaria.
=> La praxis intencional puede referirse a un proyecto individual o colectivo.
  - Siempre valorada por sus resultados.

### La praxis intencional

Si el hombre es ser práctico, entonces la historia es historia de su praxis.   <
  - Actividad consciente del sujeto que aspira concretar sus intenciones.

Praxis inintencional: cuando no existió la intención de un nuevo producto.
  - Como sucede en muchos cambios del orden económico social.

### Los hombres, sujetos de la historia

En la historia no sucede nada que no extrañe la invervención del sujeto.
  - La historia es hecha por los hombres.
    - Historia de las transformaciones.
=> El hombre como sujeto de la historia = ser en y por la historia.
  - La historia y el sujeto humano son una misma realidad.
    - El individuo no es inalterable.
    - El individuo no en un sentido atomista.
    - El individuo no es mero soporte de lo social.
    - El individuo no es efecto de lo social.

### Individualidad y socialidad

Lo social no es un prodcuto de los individuos; los individuos son un producto
social.
  - La individualidad esstá condicionada histórica y socialmente.              <
    - No determina totalmente, sino en sus formas fundamentales.
    - No puede abstraerse lo uno de lo otro.
    - La sociedad como forma por la que se desplega la actividad individual.
=> Permite la trascendencia de los actos individuales como praxis común.
  - No es intersubjetividad, sino objetividad.

### Productos inintencionales de una praxis intencional

La praxis individual también conduce a resultados no esperados.
  - Doble faceta:
    1. La praxis es intencional cuando el individuo persigue un determinado fin.
    2. La praxis es inintencional cuando esta actividad individual adopta una
       forma social.

### Racionalidad y teleología históricas

Concepción teleológica: la inintencionalidad no es propia del hombre, sino
relacionada a una potencia suprahumana.
  - Asegura la racionalidad de la praxis.
    - La forma más perfecta yace en Hegel.
=> La racionalidad es inseparable de la finalidad universal.

### La racionalidad de la historia real

Marx ha combatido la concepción teleológica por:
  1. Busca un sujeto fuera de la historia del hombre.
  2. Reduce el verdadero sujeto de la historia.
  3. Considera que la historia tiene un fin.
  4. Descansa en la racionalidad.
=> El marxismo rechaza toda visión profética o teleológica de la historia.
  - La razón no descansa en la finalidad de las acciones.                      <

La razón de la historia aparece con la historia de su razón.
  - La historia como racional tiene que integrar tanto la praxis intencional
    como la inintencional.

Si la racionalidad no está trazada de antemano ni forma parte esencial del 
hombre, entonces yace en la estructura social.
  - Implica que cualquier formación económico-social tiene su propia 
    racionalidad.

### Racionalidad universal de la historia

La racionalidad objetiva requiere de un proceso, no es espontánea.
  - Tarea científica del materialismo histórico.
    - No reducible al análisis de la racionalidad capitalista.

### La racionalidad estructural de la historia

La racionalidad de la historia de la praxis humana es universal.
  - Todo cambio tiene razón de ser.
  - No hay sociedades privilegiadas por su racionalidad.
  - La «irracionalidad» es una función ideológica.
  - La racionalidad se descubre cuando la historia no se trata como estructura.
    - Puede ser universal o nacional.
    - Puede contener otras estructuras, dependientes o no.
  => La estructura central es lo económico en que produce relaciones sociales y
     es condición de todo tipo de producción.
    - Aunque no sea el papel principal.

### Racionalidad de los cambios de estructura social

La racionalidad se encuentra en las estructuras pero también en los procesos de
aparación de otras estructuras.
  - El proceso acontece cuando las fuerzas productivas llevan al límite a los
    modos de producción hasta generar un cambio radical y la aparición de una
    nueva estructura.
    - El cambio ya está determinado por la estructura existente.
    - Hay continuidad y ruptura entre estructuras.
=> No hay lugar para:
  - Historicismo absoluto.
  - Teoricismo histórico.

### Productos históricos inintencionales

Lo queel hombre hace no siempre responde a sus intenciones:
  - Nadie ha creado una nueva estructura a partir de un proyecto individual o
    común,
    - Las consecuencias fundamentales escapan a su conciencia.

### Dualidad de la praxis individual

Los resultados de la praxis común no pueden ser referidos a una conciencia o su
suma.
  - No está al nivel dela praxis individual.
  - Sí está al nivel estructural y sus cambios.
=> Racionalidad universal, estructural y objetiva.

La práctica individual tiene un doble carácter:
  1. Intencional: acción de realización de sus propios fines.
  2. Inintencional: acción cuyo resultado social no responde a sus propios 
     fines.
=> Contradicción que muestra que el individuo no es mero soporte.

### De las praxis intencionales a la praxis común intencional

La praxis común inintencional implica la intervención consciente de los 
individuos.
  1. Los individuos actúan según sus fines.
  2. Estos fines se entrecruzan y se oponen entre sí.
  3. Los resultados no corresponden a los fines deseados.
  4. Los fines se funden en una media común a todos los individuos.
=> Problemas:
  1. No explica por qué las praxis individuales entran en conflicto.
  2. No indica por qué el resultado común es una media.
=> Es la estructura la que determina la contradicción y el resultado común.
  - Solo explicable mediante las leyes (racionales) que rigen la estructura,
    - Determina el carácter intencional o no de la praxis histórica y las leyes
      de esta estructura.

### Intereses y estructura social

El análisis de la actividad no ha de partir de los móviles ideales, sino de las
condiciones objetivas; es decir, de los intereses.
  - De los intereses ver el fin.
  - Determina las posibilidades de la praxis.

Tipos de intereses:
  - Personales = práctica individual.
  - De clase = práctica común intecional.
=> La última exige la superación de la primera, la cual es la más inmediata y
   cognoscible.
 - No nace espontáneamente.
 - Su conciencia exige un proceso.

La conciencia de clase es conciencia social limitada.
  - Solo la conciencia de clase del proletariado es conciencia de la sociedad
    entera.
=> La intencionalidad solo surge cuando hay conciencia.

### Límites de una praxis intencional colectiva

Los intereses y la conciencia no pueden impulsar una práctica que sea
contradictoria a las leyes.
  - Sería de carácter inintencional.

La praxis intencional colectiva y condicionada a la propiedad privada tiene sus
límites.
  - No es posible una praxisintencional de la sociedad entera.
=> Solo se enmienda con unanueva estructura.

### Subjetivismo y voluntarismo en la praxis histórica intencional

Por primera vez hay praxis intencional para el cambio de estructura.
  - Lay leyes no dependen de la voluntad o conciencia de los hombres.
    - No son subjetivas ni voluntarias.

### Factor subjetivo y racionalidad objetiva

La subjetividad y voluntad posible bajo los límites racionales de la estructura.

### Racionalidad y finalidad en la praxis histórica

La historia humana es un proceso histórico sujetoa leyes; es decir, racional.
