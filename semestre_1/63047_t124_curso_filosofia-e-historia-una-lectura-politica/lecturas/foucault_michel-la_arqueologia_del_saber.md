# *La arqueología del saber*, Michel Foucault

## Introducción

Los historiadores:
  - Se fijan en largos periodos de tiempo.
  - Buscan regulaciones constantes que fueron cubiertos por una capa de
    acontecimientos.
=> Disponen de instrumentos para llevar a cabo esta labor.
  - Permiten hacer distinciones que conforme se desciende, se hacen cada vez
    más amplias.

La historia de las ideas, de las ciencias, de la filosofía y de la literatura:
  - Escapan a gran parte de los historiadores.
  - Su atención se enfoca a fenómenos de ruptura.
    - Por debaoj de vastas unidades.
  - Actos y umbrales epistemológicos.
    - Interrupciones de naturaleza muy diversa.
    - Escindidos de su origen científico.
    - Purificados de complicidades imaginarias.
    - Señalan un nuevo tipo de racionalidad.
  - Desplazamiento y transformaciones de conceptos.
    - La historia de un concepto no es una racionalidad creciente.
    - Aumenta sus reglas sucesivas de uso.
  - Escalas micro y macroscópicas de la historia de las ciencias:
    - Un mismo descubrimiento u obra no puede ser descrito de la misma manera
      en uno u otro nivel.
  - Redistribuciones recurrentes que hacen aparecer varios pasados:
    - Las descripciones históricas se ordenan necesariamente en la actualidad
      del saber.
  - Unidades arquitectónicas de los sistemas.
    - La descripción de continuidad culturales no es pertinente.
    - Descripción de las coherencias internas.
=> Trabajo de transformación teórica.
  - Funda una ciencia, se desprende de la ideología de su pasado a la vez que
    lo muestra como tal.
  - Análisis literario que se fija en la estructura propia de una obra, de un
    libro o de un texto.
    - No época, movimiento, autor o su creación.
=> El gran problema a plantearse:
  - No es cómo el saber ha establecido continuidades, un horizonte único y su
    modo de transmisión implicados.
  - El problema es del recorte y del límite, de las transformaciones que valen
    como fundación y renovación de las fundaciones.
=> Búsqueda de discontinuidades.
  - La historia a secas busca lo contrario: la continuidad.

No se crea que estas dos grandes formas de descripción:
  - Han pasado de lo continuo a lo discontinuo y visceversa.
  - Se han cruzado sin reconocerse.
=> Son los mismos problemas pero con efectos inversos.
  - Resumen: la revisión del valor del documento.

La historia ha utilizado el documento para a partir de él reconstruir el pasado
que emanan y queha quedado desvanecido detrás de ellos.
  - Se ha cambiado la postura ante el documento.
    - Yo no se trata de interpretarlo y determinar si es veraz.
    - Se trata de trabajarlo desde el interior y elaborarlo.
=> Ya no es materia inerte para reconstrucción, sino la definición de un tejido
   que describe relaciones.
  - La historia como una forma de dar estatuto a una masa de documentos que no
    se separa de una sociedad.

Tradicionalmente la historia se dedica a memorizar monumentos del pasado.
  - A transformarlos en documentos.

Actualmente la historia es un despliegue de elementos que se han de agrupar y
relacionar.
  - La transformación de documentos a monumentos.
=> Si la arqueología, como disciplina de los monumentos mudos, adquiría un
   sentido por la restitución de un discurso histórico; entonces en nuestros
   días la historia tiende a la arqueología como descripción intrínseca del
   documento.
  - Consecuencias:
    1. - Multiplicación de las rupturas en la historia de las ideas,
         desgajamiento para constituir series.
       => Los periodos largos de la historia no es una vuelta a las filosofías
          de la historia o a fases prescritas sino la elaboración metodológica
          de las series.
       - Vuelve a poner en la mesa los temas de la convergencia y la 
         realización: puesta en duda de la totalización.
       => Irreductibilidad a un modelo general de una conciencia que adquiere,
          progresa y recuerda.
    2. - La noción de discontinuidad ocupa un mayor lugar en las disciplinas
         históricas.
       => Constituye una operación deliberada del historiador, que también es
          resultado de su descripción y es el concepto que el trabajo no deja de
          especificar (no es siempre la misma discontinuidad): la discontinuidad
          como noción, instrumento y objeto.
          - El desplazamiento de lo discontinuo.
          => De fatalidad a operatividad, de lo negativo a lo positivo.
    3. - La historia global empieza a borrarse y la historia general empieza a
         esbozarse.
       => Problematización de las tres hipótesis de la historia global:
          suposición que todos los acontecimientos de un área espacio-temporal
          se puede establecer un sistema de relaciones homogéneas, que la única
          forma de historicidad arrastra al resto de las estructuras, y que la
          historia puede articularse en grandes unidades.
       - La historia general determina cuáles relaciones son legítimas.
       => Qué cuadro es posible construir, el despliegue de una dispersión.
    4. - El encuentro de nuevos problemas metodológicos, los cuales ya existían.
       => Constitución del *corpus*, el principio de elección, la definición del
          nivel de análisis, la especificación del método de análisis
          cuantitativo de los datos, la delimitación de los conjuntos y la
          determinación de relaciones que permiten caracterizar a un conjunto.

Las cuatro consecuencias forman parte de la metodología de la historia por:
  1. Se libra de la filosofía de la historia y lo que esta planteaba: teleología
     racional, relatividad del saber histórico y la posibilidad de constituir
     un sentido.
  2. En algunos puntos se reproducen problemas de otras disciplinas.
    - Puede llamarse estructuralismo pero considerando:
      - No cubren el campo metodológico de la historia.
      - En la mayoría de los casos los análisis no fueron importados de otros
        campos.
    => No es oposición entre la «estructura» y la «historia».

La mutación epistemológica data desde Marx.
  - Pero se ha tardado la producción de sus efectos, porque son la cara de un
    sistema de pensamiento donde el análisis histórico es el discurso del
    contenido y la conciencia humana, el sujeto originario de todo devenir:
    el tiempo como totalización.
=> Tema recurrente desde el siglo XIX: salvar la soberanía del sujeto y las
   figuras de la antropología y el humanismo.
  - Descentramiento de Marx => historia global y el establecimiento de un
    sistema de valores.
  - Descentramiento de genealogía nietzscheana => Búsqueda de un fundamento
    originario que hace la racionalidad el *telos* de la humanidad.
  - Descentramiento del psicoanálisis, la lingüística y la etnología => la
    historia como dinamismo interno, como trabajo de la libertad o conciencia
    recobrándose a sí misma.
    - Apertura viva de la historia dejando de lado todos los problemas
      metodológicos.
=> Función conservadora:
  - Totalidades culturales que critican a Marx.
  - Búsqueda de lo primigenio opuesto a Nietzsche.
  - Historia viva, continua y abierta.

Para la función conservadora la discontinuidad y la diferencia implica el
asesinato de la historia.
  - Pero no es desaparición de la historia, sino a la forma de historia que se
    refería a la actividad de un sujeto.
    - Se llora que la conciencia no tiene un abrigo más seguro.
    - Se llora la pérdida de sistema.
    - Se llora la imposibilidad de uso ideológico de la historia.
  => Se pierde la historia como sueño tranquilizador de un sujeto atormentado.

Objetivo del libro:
  - Revisión de los métodos de la historia.
  - Desatar las últimas sujeciones antropológicas.
  - Mostrar cómo se produjeron esas sujeciones.
=> Algo ya presente en *Historia de la locura*, *El nacimiento de la clínica* y
   *Las palabras y las cosas*.
  - No tenían coherencia.

Algunas observaciones:
  - No se trata de transferir el dominio de la historia un método
    estructuralista, sino una transformación autóctona.
  - No es la utilización de categorías de las totalidades culturales, sino
    revisión de teleologías y totalizaciones.
  - Se trata de definir un método de análisis histórico liberado del tema
    antropológico que:
    - Formule los instrumentos.
    - Refuerce los resultados obtenidos.
=> No está en el campo de la estructura, sino en el campo donde se entrecruzan
   el ser humano, la conciencia, el origen y el sujeto.
  - No es repetición de obras pasadas, sino corrección y crítica interna.
  => Un libro con muchas cautelas.
    - No se pida invariabilidad.

## III. La descripción antropológica

### 1. Arqueología e historia de las ideas

El aparato creado en las secciones anteriores es engorroso:
  - Ya existen bastantes métodos para analizar el lenguaje.
  - Desconfianza de las unidades del discurso como «libro» u «obra».
  - No están claras las diferencias entre los instrumentos creados de las
    figuras de las que partieron.
=> ¿Qué ofrece la arqueología que sea algo distinto?
  - ¿En qué se diferencia de la historia de las ideas?

La historia de las ideas es difícil de caracterizarse.
  - Objeto incierto.
  - Fronteras mal dibujadas.
  - Métodos tomados de acá y allá.
  - Marcha sin rectitud o fijeza.
=> Se le reconocen dos papeles:
  1. Historia de los anexos y los márgenes.
    - Historia de los conocimientos imperfectos.
    - Historia de las filosofías presentes en otras áreas o en la vida
      cotidiana.
    - Historia de tematismos que nunca se han cristalizado en sistemas.
    - Historia de la subliteratura.
  => Se dirige a lo insidioso y a las representaciones anónimas.
    - Análisis de las opiniones más que del saber.
  2. Historia que atraviesa disciplinas existentes para tratarlas y
     reinterpretarlas.
    - Un estilo de análisis.
    - Toma la historia de las ciencias, las literaturas y las filosofías para
      descubrir ideas que les han servido de fondo empírico y no reflexivo.
    - Sigue la génesis de sistemas u obras.
    - Muestra cómo los temas se desenlazan.
  => Disciplina de los comienzos y de los fines, de las continuidades oscuras
     y los retornos, de la descripción del juego de cambios y de intermediarios.

Los dos papeles se articulan uno sobre otro.
  - De forma general, la historia de las ideas describe el paso de lo
    no-filosófico a la filosofía, de la no-cientificidad a la ciencia, de la
    no-literatura a las mismas obras.
  - Grandes temas que la ligan con el análisis histórico: génesis, continuidad
    y totalización.

La arqueología es un abandono de la historia de las ideas para hacer una 
historia distinta que presenta cuatro diferencias.
  1. Asignación de la verdad. No pretende definir lo que está debajo de un
     discurso, sino esos discusos en tanto prácticas que obedecen a reglas.
    - El discurso no es *documento* sino *monumento*, no es signo de otra cosa,
      sino su propio volumen.
    - No interpreta y no busca ser alegórica.
  2. Análisis de las contradicciones. No busca una transición que une discursos
     y no va en progresión lenta de la opinión a la estabilidad de la ciencia,
     sino que busca el juego de reglas que atraviesan un discurso.
    - No es «doxología» sino análisis diferencial.
  3. Descripciones comparativas. Su enfoque no es la obra y lo que esto implica,
     sino las reglas de prácticas discursivas que atraviesan a unas obras y
     que a veces los gobiernan por entero o solo en parte.
    - Le es ajeno el sujeto creador como razón de una obra y el principio de 
      unidad.
  4. Localización de las transformaciones. No restituye lo experimentado en el
     instante mismo del discurso, sino una reescritura de lo que ha sido.
    - No es vuelta al origen, sino descripción de un discurso-objeto.

> Pŕactica discursiva: conjunto de reglas anónimas, históricas, siempre
> determinadas en el tiempo y el espacio, que han definido en una época dada,
> y para un área social, económica, geográfica o lingüística dada, las
> condiciones de ejecicio de la función enunciativa. [Segunda parte 
> —«El enunciado y el archivo»—, tercer capítulo —«La descripción de los
> enunciados»].

### 2. Lo original y lo regular

La historia de las ideas trata de los discursos como un dominio de dos valores:
antiguo o nuevo, inédito o repetido, tradicional u original…
  - Dos categorías de formulaciones:
    1. Las que no tienen antecedentes y son relativamente pocas.
      - Historia de las invenciones y despertares de la conciencia.
      - El historiador busca discutir la línea continua de una evolución.
      - Restitución de verdades.
    2. Las triviales y masivas.
      - Historia como inercia y acumulación del pasado.
      - El historiador pretende describir figuras globales.
      - Restablecimiento de solidaridades olvidades.

En la historia de las ideas se realizan relaciones entre ambos análisis, nunca
están en estado puro.
  - No impide un análisis bipolar de lo antiguo y lo nuevo.
  - Pone en juego el elemento empírico de la historia y la problemática del
    origen.
=> Plantea dos problemas metodológicos:
  1. El de la semejanza.
  2. El de la precesión.

La precesión no es un dato irreductible y primero.
  - No es medida absoluta.
  - Los antecedentes no bastan e incluso subordina el discurso que analiza.
=> Relativa a los sistemas de discurso que pretende analizar.

La semejanza implica un senido y un criterio.
  - La identidad no es criterio.
  - La semejanza no es inmediatamente reconocible.
=> Su analogía es un efecto del campo del discurso en donde se la localiza.

No es legítimo exigir originalidad a los textos.
  - Solo tiene sentido en series muy bien definidas.
  - Es algo entretenido pero tardío.
=> La oposición originalidad-trivialidad no es pertinente.
  - Lo que se busca es establecer la regularidad de los enunciados.
    - No se opone a la irregularidad de otro enunciado, sino a otras 
      regularidades.

> Enunciado: unidad elemental del discurso que carece de criterios
> estructurales de unidad ya que se trata de una función [Segunda parte 
> —«El enunciado y el archivo»—, primer capítulo —«Definir un enunciado»].
> Característica de la función enunciativa: materialidad repetible por la cual
> un enunciado no es algo dicho de una vez y para siempre sino que se convierte
> en un tema de apropiación o de rivalidad. [Segunda parte  —«El enunciado y 
> el archivo»—, segundo capítulo —«La función enunciativa»].

La arqueología no se enfoca en lo original o repetido, sino en la regularidad.
  - Poner al día la regularidad de una práctica discursiva.
  - La regularidad no es menos operante en la operación.
  - No hay diferencia entre enunciados creadores y los imitativos.
  - Es un dominio continuamente activo.

Direcciones en las que se abren las regularidades enunciativas:
  1. *Homogeneidad enunciativa*. Es distinta a la analogía lingüística
     —traductibilidad— y de la identidad lógica —equivalencia. Permite mostrar
     una nueva práctica discursiva aunque exista analogía o equivalencia, o
     incluso cuando nada de eso existe.
    - Existe entrecruzamiento de los tres elementos.
  => Implica un número de relaciones e interdependencias que tienen que ser
     analizados.
  2. *Jerarquía enunciativa*. Las reglas enunciativas no están presentes en un
     solo enunciado, sino en grupos donde hay reglas más generales y otras más
     específicas, que permiten un árbol de derivación enunciativa de un
     discurso.
    - La derivación de enunciados rectores no es deductiva ni germinación de una
      idea general ni génesis psicológica; no es del orden de las 
      sistematicidades ni de las sucesiones cronológicas.
  => Implica paralelismos entre el orden arqueológica y el orden sistemático
     o de sucesiones cronológicas que deben ser analizados.
=> La arqueología describe campos de homogeneidad enunciativa que tienen su
   propio corte temporal y unas jerarquías que evitan una sincronía masiva.
  - De la «época» surgen específicamente los «periodos enunciativos».

### 3. Las contradicciones

La historia de las ideas de ordinario busca un principio de cohesión que
restituya una unidad oculta.
  - No suponer que el discurso está minado de contradicciones.
=> Pero la coherencia es también el resultado de la investigación.
  - La supone para reconstruirla.

Existen muchos medios para encontrar la coherencia.
  - No contradicción lógica.
  - Hilo de las analogías y de los símbolos.
  - Grado de explicitación.
  - En el plano del individuo y su circunstancia, o la sociedad y culturas.
=> Pretende demostrar que las contradicciones visibles solo son superficiales.
  - Reducción de los destellos en un foco único.
  - La contradicción como ilusión de una unidad.

Al término queda:
  - Contradicciones residuales.
  - Contradicción fundamental.
    - Irreductible y como principio organizado que da cuenta de las demás
      contradicciones.
    - La contradicción funciona como principio de historicidad.
=> Dos niveles de contradicciones:
  1. El de las apariencias que se resuelven en una unidad profunda del discurso.
  2. El de los fundamentos que dan lugar al mismo discursos.
=> Analizar el discurso es igual a desaparecer y reaparecer las contradicciones.

En el análisis arqueológico no se consideran las contradicciones de esta manera:
ni apariencias a superar ni principios secretos a despejar.
  - Son objetos por sí mismos.
  - Mostrar el lugar común de dos elementos en contradicción.
  - Creación de un cuadro regular donde la creación es posible.
=> La contradicción no se resuelve ni tiene punto de conciliación ni se 
   transfiere a un campo más fundamental.
  - Trata de determinar la medida y la forma de su desfase.
  - Describe los diferentes espacios de disensión.

La arqueología renuncia a tratar a la contradicción como una función general:
la trata a modo de tipos, niveles y funciones.
  - Diferentes tipos:
    1. *Contradiciones derivadas*: se localizan en el campo de las proposiciones
       o aserciones y no afectan el régimen enunciativo, y que contituyen un
       estado terminal —*terminus ad quem*—.
    2. *Contradicciones extrínsecas*: remiten a la oposición entre formaciones
       discursivas, a partir del cual —*terminus a quo*— traspasan los límites
       de la formación y oponen tesis que no dependen de las condiciones de
       enunciación.
    3. *Contradicciones intrínsecas*: se despliegan en la misma formación
       discursiva y hace surgir subsistemas, que no tienen un estado terminal
       al mismo tiempo que son derivamos.
    => Las pertinentes para el análisis arqueológico.
  - Diferentes niveles:
    1. Inadecuación de los objetos.
    2. Divergencia de las modalidades enunciativas.
    3. Incompatibilidad de conceptos.
    4. Exclusión de opciones teóricas.
  => Demuestra que las contradicciones intrínsecas no son hechos puros y 
     simples.
  - Diferentes funciones:
    1. Función de desarrollo inicial: abren secuencias, definen nuevos conceptos
       o modifican el campo sin modificar la positividad del discurso.
    2. Función de reorganización: posibilidad de traducción de enunciados,
       de organizar conceptos con otra estructura y de articulación en otro
       espacio sin que modifique las reglas de formación.
    3. Función crítica: ponen en juego la existencia del discurso al definir
       su imposibilidad efeciva o de retroceso histórico.
=> El análisis arqueológico da primacía a la contradicción para ver cómo se
   constituyen, qué forma adoptan, las relaciones que tienen entre sí y el
   dominio que rigen.
  - Mantiene al discurso en sus contradicciones.

> Positividad: análisis de una formación discursiva como análisis de la rareza,
> de las relaciones de exterioridad y de las acumulaciones, en lugar de la
> búsqueda de las totalidades, del fundamento trascendental o del origen.
> [Segunda parte —«El enunciado y el archivo»—, cuarto capítulo —«Rareza,
> exterioridad y acumulación»].

### 4. Los hechos comparativos

El análisis arqueológico individualiza y describe las formaciones discursivas:
las compara.
  - No es lo mismo a las descripciones epistemológicas o «arquitectónicas»
    porque no analizan la estructura interna de una teoría.
  - Es plural y analiza yuxtaposiciones, separaciones o enfrentamientos.
  - Puede dirigirse a un tipo de discurso o mediante aproximación lateral.

El análisis es diferente a los que se practican de ordinario porque:
  1. La comparación es limitada y regional. Se buscan configuraciones singulares
     como formaciones discursivas que a su vez se relacionan con otras formas
     de discurso para reconocer una configuración interdiscursiva.
    - No es descripción de totalidades culturales como la *Weltanschauung*.
    - Más que lagunas o errores en el análisis es exclusión deliberada y 
      metódica.
    - El número de relaciones no está determinado de antemano.
    - La formación discursiva no forma parte de un solo campo de relaciones ni
      ocupa o ejerce la misma función.
    - No es una ciencia, racionalidad, mentalidad o cultura.
    - Es análisis comparado que no reduce o totaliza a los discursos: no unifica
      sino multiplica.
  2. Se mantienen las analogías y las diferencias como aparecen en las reglas de
     formación, lo que implica cinco tareas:
    1. Mostrar *isomorfismos arqueológicos* entre formaciones diferentes.
      - Cómo pueden ser formados a partir de reglas análogas.
    2. Definir el *modelo arqueológico* de cada formación.
      - En qué medida las reglas se aplican o no según el modelo y los distintos
        tipos de discurso.
    3. Mostrar *isotopías arqueológicas*.
      - Cómo conceptos diferentes pueden ser análogos.
    4. Indicar los *des-fases arqueológicos*.
      - Cómo una misma noción puede englobar dos elementos arqueológicos
        distintos.
    5. Establecer las *correlaciones arqueológicas*.
      - Cómo puede establecerse una positividad u otras relaciones
        complementarias o subordinadas.
  3. Poner de manifiesto las relaciones entre formaciones discursivas y dominios
     no discursivos —p. ej. instituciones, acontecimientos políticos o procesos
     económicos.
    - No pregunta por motivos ni lo que se expresa en ellos.
    - Trata de definir formas específicas de articulación.
  => Los fenómenos de expresión son efectos de una lectura global que busca
     analogías formales y traslaciones de sentido; las relaciones causales solo
     son al nivel de contexto y su efecto sobre el sujeto parlante.
    - Solo localizables cuando han sido definidas las positividades y han sido
      formadas las reglas.
=> Se busca descubrir el dominio de existencia y funcionamiento de una práctica
   discursiva.
  - Su dimensión es la historia general.
  - Mostrar cómo la autonomía del discurso no lo hace ideal o históricamente
    independiente; al contrario: scar a la luz lo que la historia puede dar
    lugar a tipos definidos de discurso con historicidades diversas.

### 5. El cambio y las transformaciones

La historia de las ideas tiene a su favor tomar en cuenta los fenómenos de
sucesión.
  - La arqueología parece tomar la historia para congelarla.
    - Descuida las series temporales.
    - No hay sincronía, sino reglas generales.
    - La cronología solo para fijar los límites de las positividades.
  => No hay ley del devenir sino intemporalidad discontinua.
=> Pero hay que contemplar las cosas más de cerca.

#### A

En primer lugar la aparente sincronía de las formas discursivas.
  - Su suspensión implica la aparición de relaciones que caracterizan la
    temporalidad de los discursos.
    1. La arqueología define las reglas de formación. Análisis del grado y la
       forma de permeabilidad de un discurso; muestra de las condiciones que
       posibilitan la correlación entre enunciados nuevos y acontecimientos
       «exteriores».
    2. No todas las reglas tienen la misma generalidad. La subordinación es
       jerárquica pero también puede tener un vector temporal; no es una red
       uniformemente simultánea ni puramente lógico ni una sucesión lineal;
       no es indiferente a lo temporal sino que localiza vectores temporales de
       derivación.
    3. La arqueología no trata como simultáneo lo que se da como sucesivo. Se
       deja en suspenso el encadenamiento de un discurso; analiza las sucesiones
       que se superponen en el discurso; no sigue un hilo sino su condición
       de posibilidad.
       - El discurso no tiene el mismo modelo de historicidad que el curso de
         la conciencia o la linealidad del lenguaje.

#### B

La arqueología habla más fácilmente de cortes y de nuevas formas de positividad
en comparación a la historia de las ideas.
  - Desenreda los hilos tendidos por los historiadores: multiplica las 
    diferencias.
=> Paradójico al hábito de los historiadores, ya que es él quien maneja las
   paradojas debido a la preocupación por las continuidades.

La arqueología no inventa diferencias, las toma en serio.
  - No las reduce ni las ve como errores como lohace la historia de las ideas.
  - Las toma por objetos cuando usualmente se consideran obstáculos.
=> Proceso de diferenciación que opera de la siguiente manera:
  1. Distinción de varios planos de acontecimientos posibles en el discurso.
    - Plano de los enunciados.
    - Plano de la aparición de los objetos.
    - Plano de la derivación de nuevas reglas de formación.
    - Plano de sustituciones discursivas.
  2. Definición de las modificaciones. Sustituye la referencia indiferenciada
     al cambio por el análisis delas transformaciones.
    - Transformación de los diferentes elementos de un sistema.
    - Transformación de las relaciones de un sistema.
    - Transformación de las relaciones entre diferentes reglas de formación.
    - Transformación de las relaciones entre diversas positividades.
  => Establecer las transformaciones en las que consiste el «cambio».
    - Le quita lo abstracto y lo vacío, las metáforas —movimiento, flujo,
      evolución— para hacerla analizable.
    - Destruye leyes universales.
  3. Análisis de fenómenos de continuidad, de retorno y de repetición. El
     cambio de formas no implica un cambio de todos los elementos, lo que
     cambia es su principio de multiplicidad y dispersión.
    - Elementos que perviven las formaciones.
    - Elementos que se modifican en las nuevas formaciones y perviven en otras.
    - Elementos que aparecen después de la formación y son las primeras en
      posteriores formaciones.
    - Elementos que reaparecen en las formaciones.
  => Encontrar su medida y explicarlos.
    - La continuidad no como un fin, sino como elemento de la práctica 
      discursiva.
  4. Descripción de la dispersión de las discontinuidades. No son homogéneas
     e indiferenciadas, sin lapso de duración o en un momento dado.
    - Las diferentes rupturas tienen importantes desfaces.
      - Desarticulación de la sincronía de los cortes.
      - No son un tope.
      - Son las transformaciones que influyen de modo general en las
        formaciones discursivas.

### 6. Ciencia y saber

Se ha pasado por alto a las ciencias, filosofías, literaturas y textos políticos
y se ha hecho hincapié en disciplinas cuya cientificidad es dudosa.
  - ¿Por qué?
=> Análisis de la relación entre la arqueología y las ciencias.

#### A. Positividades, disciplinas, ciencias

Acuse. La arqueología con sus «formaciones discursivas» y «positividad» describe
seudociencias, ciencias en estado prehistórico o penetradas de ideología.
=> La arqueología no describe disciplinas.
  - Aunque pueden servir en la descripción.

Acuse. Las positividades son dobletes de las disciplinas y las formaciones
discursivas son esbozos de ciencias futuras.
=> Las positividades ignoran muchos análisis disciplinarios por lo que las
   formaciones discursivas no son las ciencias futuras.

Acuse. No hay ciencia donde hay positividad, no es una relación cronológica,
sino una alternativa.
=> Las formaciones discursivas no se identifican con las ciencias ni con las
   disciplinas.
  - ¿Cuál es la relación entre las positividades y las ciencias?

#### B. El saber

Las positividades no caracterizan:
  - Formas *a priori* del conocimiento.
  - Formas de racionalidad puestas en acción.
  - Estados de los conocimientos en un momento dado del tiempo.
=> Intenta mostrar con qué regla una práctica discursiva puede formar grupos de
   objetos y de conceptos.
  - No es una ciencia porque no tienen una estructura definida ni estricta.
  - No es conocimiento aglutinado.
  - Son elementos por los que se pretende construir proposiciones coherentes,
    descripciones exactas, verificaciones o teorías.
=> Lo previo que se constituirá como conocimiento o ilusión.

El carácter previo implica:
  - No puede asumirse como un dato inmerso en lo imaginario que se retoma en la
    forma de racionalidad.
  - No es un preconocimiento, un estadio arcaico.
  - Es una práctica discursiva por la que eventualmente se constituye un 
    discurso *si seda el caso*.
=> Se le puede llamar saber.
  - Lo que es posible hablar de una práctica discursiva.
  - El dominio constituido de diferentes objetos que adquirirán o no un estatuto
    científico.
  - El espacio en el que el sujeto puede tomar posición para hablar de los
    objetos que compone su discurso.
  - El campo de coordinación y subordinación en que los conceptos aparecen, se
    definen, se aplican y se transforman.
  - Define posibilidades de utilización y de apropiación ofrecidas por el
    discurso.
=> El saber no depende de las ciencias, pero su existencia solo es posible
   mediante la práctica discursiva, que a su vez es definida por el saber que
   forma.
  - La ciencia tiene un fondo de saber.

<!-- Aquí se puede entender el título del libro: el análisis arqueológico se
     perfila al saber porque este proviene de una práctica discursiva
     independiente o que yace de fondo al discurso científico: la posibilidad,
     el lugar y el dominio para un discurso racional o no. -->

El eje de la arqueología:
  - Es práctica discursiva-saber-ciencia.
  - No es conciencia-conocimiento-ciencia.
=> Esto implica:
  - Diferencia al respecto de la historia de las ideas, ya que el análisis de la
    segunda se equilibra en el conocimiento.
  - Su equilibrio es el saber, donde el sujeto es situado y dependiente, nunca
    un titular.
=> Necesidad de distinguir entre *dominios científicos* y 
   *territorios arqueológicos*
  - Dominio científico. Proposiciones con ciertas leyes de construcción, por lo
    que quedan excluidas las verdades que no nacen de esa sistematicidad.
  - Territorios arqueológicos. Puedes atravesar textos literarios, filosóficos
    o científicos donde el saber puede ser demostración pero también ficción,
    reflexión, relato, reglamento institucional o decisión política.
=> Problemas sin resolver:
  - El lugar del dominio científico en el territorio arqueológico.
  - El modo como emerge el dominio científico del territorio arqueológico.

<!-- Aquí también hay dos cuestiones a plantear:
    1. Pregunta abierta. ¿Qué relación o qué impacto puede tener el análisis
       arqueológico en un contexto político?
       - Considerar lo que viene en el último apartado; a saber, el F.
    2. Hipótesis a discutir. Las implicaciones que tiene la noción de dominio
       científico y territorio arqueológico en la intencionalidad del texto
       filosófico, el filosofar y la filosofía.
      - El «juego» es muy utilizado, ¿cómo tiene cabida en el discurso
        filosófico en contraste a la necesidad de «rigurosidad» en este? -->

#### C. Saber e ideología

La ciencia una vez constituida no absorbe toda la práctica discursiva por la que
surge, ni disipa el saber que la rodea.
  - Más bien se localiza en un campo del saber y desempeña un papel en él.
=> Hay una relación entre el saber y la ciencia que la arqueología en lugar de
   buscar exclusiones o sustracciones, debe dar cuenta de cómo se inscribe la
   ciencia en el elemento del saber.
  - Es el espacio de juego donde se especifican las relaciones de la ideología
    con las ciencias.

La articulación de la ideología en las ciencias:
  - No se da en el plano de la estructura ideal.
  - No acontece en su utilización técnica por una sociedad.
  - No acaece en la conciencia de los sujetos que la construyen.
  - Se constituye en la perfilación sobre el saber.
=> La cuestión ideológica es como práctica discursiva y su funcionamiento con
   otras prácticas, de lo que se obtiene que:
  1. La ideología no es exclusiva de la cientificidad.
  2. Las contradicciones y defectos teóricos pueden señalar el funcionamiento
     ideológico de una ciencia.
  3. La corrección de errores no implica la separación de la ciencia de la
     ideología.
  4. Ocuparse del funcionamiento ideológico no es sacar a la luz sus
     presupuestos filosóficos.
    - No es ir a los fundamentos, sino ponerla a discusión.

#### D. Los diferentes umbrales y su cronología

Cuatro umbrales:
  1. Umbral de la positividad: cuando una práctica discursiva se individualiza
     y adquiere su autonomía, cuando se encuentra actuando un solo sistema de
     formación de los enunciados o cuando este sistema se transforma.
  2. Umbral de epistemologización: cuando un conjutno de enunciados se recorta
     y pretende hacer valer unas normas que ejercen una función dominante sobre
     el saber.
  3. Umbral de cientificidad: cuando la figura epistemológica obedece a 
     criterios formales que responden tanto a reglas arqueológicas de formación
     como a leyes de construcción de preposiciones.
  4. Umbral de formalización: cuando el discurso científico define sus axiomas
     necesarios, los elementos que utiliza, las estructuras proposicionales que
     le son legítimas y las transformaciones que acepta; es decir, cuando puede
     desplegar un edificio formal.
=> Su cronología es uno de los dominios de la exploración de la arqueología.
  - No es regular ni homogénea.
  - No es un andar en un solo sentido, se pueden franquear unos umbrales sin
    otros.
  - No es un proceso evolutivo.
=> La división histórica de la ciencia solo es entre lo que es y no es 
   científico.
  - Solo en las matemáticas no pueden distinguirse los umbrales por darse de
    golpe.
    - Motivo por el que tomó relevancia como modelo de cientificidad.
    - Motivo por el cual su generalización es peligrosa.

#### E. Los diferentes tipos de historia de las ciencias

Los múltiples umbrales permiten diferentes análisis históricos.
  - El situado en la formalización. Solo se regla en supropio campo donde el
    pasado se muestra como ingenuidad y formalización progresivas: análisis
    recurrencial.
    - El ejemplo es la historia de las matemáticas.
  - El situado en el umbral de la cientificidad. Descripción dela depuración de
    un concepto en concepto científico, de cómo la ciencia se establece sobre
    lo precientífico, cómo ha franqueado obstáculos, donde la norma es la
    ciencia constituida para mostrar las oposiciones entre la verdad y el error.
    - El ejemplo es la historia epistemológica de las ciencias.
  - El situado en el umbral de la epistemología. Se busca evidenciar cómo las
    prácticas discursivas que dan lugar al saber toma estatuto de ciencia, cómo
    la instauración de la ciencia yace en la posibilidad de la formación
    discursiva y las modificaciones de su positividad, sin recurrir a lo 
    profundo, lo originario o la experiencia vivida, sino al juego de las
    diferencias y las relaciones.
    - El ejemplo es la historia arqueológica.
  - El situado entre los umbrales. Ve las relaciones que unen a cada umbral a
    partir de las prácticas discursivas que les dan lugar.
  => Este conjunto de relaciones es la *episteme*.
    - No es una visión del mundo.
    - No es un estadio general de la razón.
    - No es una forma de conocimiento.
    - No es unidad de un sujeto, espíritu o época.
    - Es un recorrido indefinido de relaciones.
    - Es el modo de aprehender las limitaciones de un discurso.
    - Es lo que posibilita a las figuras epistemológicas y las ciencias.
    - Es lo que está sujeto a procesos de una práctica histórica.
    - Es lo que pone en juego el hecho de que la ciencia existe, no si tiene
      derecho a ser ciencia.

#### F. Otras arqueologías

Las arqueologías no tienen por qué limitarse a los discursos científicos.
  - Una orientación puede ser la sexualidad como campo de enunciaciones posibles
    o verdades.
    - Se dirige más a la ética que a la episteme.
  - Otra orientación puede ser la creación artística como práctica discursiva
    que toma cuerpo en las técnicas y sus efectos.
    - La obra como atravesada por la positividad de un saber.
  - Una más en torno al saber político como campo de diferentes prácticas en
    las que se encuentra su espeficidad.
    - La política no como teorización de la práctica ni como aplicación de la
      teoría.
    - La política sin necesidad de pasar por la conciencia individual o 
      colectiva y, por ende, tampoco sobre la conciencia revolucionaria.
=> La arqueología no busca describir la estructura específica de la ciencia,
   sino la del saber.
  - Solo se ha explorado la orientación a la episteme ya que en nuestras 
    culturas las formas discursivas tienden a epistemologizarse.
  - La episteme no es un dominio obligado.
