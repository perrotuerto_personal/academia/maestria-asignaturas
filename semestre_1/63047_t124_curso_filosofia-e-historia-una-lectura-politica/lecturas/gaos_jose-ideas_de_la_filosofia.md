# *Ideas de la filosofía*, José Gaos

## El historicismo y la enseñanza de la filosofía

La enseñanza de la filosofía pone en práctica muy variados planes.
  - Como un sistema: la historia de la filosofía.
    - Sistema perenne.
    - Sistema renovado.
    - Sistema original y actual.
  - Elegimos por aptitud:
    - Propedéutica.
    - Normativa.
    - Práctica
  - En la enseñanza histórica se opta por:
    - Los orígenes de la filosofía occidental.
    - Los orígenes de la filosofía general.
    - La filosofía de grandes pensadores.
    - La historia entera de la filosofía.
  => Contenido de la enseñanza.
  - Lección o conferencia.
  - Lección y explicación, o comentarios del texto.
  - Ejercicios.
  - Diálogos.
  => Forma de la enseñanza.

En la actualidad la tendencia es a la enseñanza histórica y textual de la
filosofía.
  - Enseñanza de la filosofía como enseñanza de la historia de la filosofía.
  - Por el auge del historicismo.

La enseñanza de la filosofía acarrea ideas sobre la verdad de la filosofía:
  - La verdad ya ha sido descubierta.
  - La verdad es histórica.
=> Implica modificación de la currícula.
  - Problemático.

La conciencia histórica tiene un gran significado en nuestro tiempo.
  - No es sobre sucesos particulares.
  - Es sobre lo universal de la cultura humana.
  - Enseñanza de la historicidad es esencia humana: el hombre
    no tiene naturaleza.
=> vs: ha estado organizada por la concepción tradicional de la verdad y
   la realidad.
  - La realidad como unidad.
  => Constitución de la «verdadera» filosofía, el resto como «falsas».

A las «falsas» filosofías les afecta lo histórico.
  - Verdad histórica.

A las «verdaderas» filosofías no les afecta.
  - Verdad trascendente.

La realidaden cuanto correlato de la humanidad también muda: es plural.
  - No hay filosofía falsa, sino todas verdaderas acorde a la verdad de
    su tiempo.
  - La pluralidad tiene unidad que permite pensar la igual verdad en todas.
  - La pluralidad es histórica de la filosofía tiene una dimensión no reducible
    a lo temporal: lo individual-personal.

La formación como incorporación de la cultura.
  - Correlación entre colectividad e individuos humanos.
  - La historicidad de la naturaleza humana es la preexistencia de la formación
    a las partes de la cultura encontradas por una persona.
    - Conexión entre lo pedagógico y lo histórico.

Lo humano es su historia por lo que la enseñanza es sobre su historia.
  - Pero en la concepción historicista es la «Historia», la *realidad* histórica
    de lo humano.
    - La filosofía es un caso particular.

La Historia tiene una función primaria narrativa de los hechos.
  - La filosofía se presenta como realidad histórica textual.
    - Es expresión ideal de la realidad.
    - Seasume con una naturaleza filológica.

La Historia no solo narra, también explica para hacer comprensibles los hechos.
  - Fundada en la relación delos objetos una totalidad.
    - Entendida de modo no filosófico o filosófico.

La Historia de la filosofía fue creada por Hegel.
  - Es una parte del sistema de la filosofía.
  - Es doxografía donde las opiniones van emergiendo unas de otras.
  - Historicidad evolutiva y autorrealización.
=> Consecuencia: imperio de la Historia doxográfica.
  - Concatenación abstracta de filosofemas.
  - Común en las obras de Historia dela filosofía.
  - La biografía como elemento no solo de localización, sino para hacer
    comprensibles los filosofemas.
  - La comprensión se reduce ala referencia entre filosofemas.
  - La manifestación extrema es la independiencia de los filosofemas de
    la realidad circundante.
    - Filosofía como un diálogo entre todos los filósofos.

En la filosofía actual:
  - Historia dela filosofía ≠ la enseñanza histórica de la filosofía.
  - Los filosofemas son correlatos del filosofar, actividad informante de la
    vida toda determinadas por su personalidad.
    - Formas *suis generis*.
    - Expresión del entorno cultural.
    => No son abstractas.

Primero consecuencia: la Historia de la filosofía no debe de omitir la
unidad orgánica de los sistemas.

Segunda consecuencia: la explicación histórica de la filosofía tiene que ir
más allá de lo abstracto para extenderse a la totalidad de la historia.
  - La Historia solo es universal.
  - La filosofía y la política en un primer plano.

La comprensión y explicación filosófica más profunda requiere considerar los
demás fenómenos no filosóficos.
  - Practicar otras disciplinas.
  - La filosofía no funciona en el vacío, sino en la experiencia de vida.

La traducción afecta la autenticidad de los textos porque es también
interpretación.
  - Supone una equivalencia expresiva.
  - Es recreación.
  - Principalmente para principiantes.

La narrativa de la Historia es selectiva.
  - Término subjetivo.

Las ideas evolucionistas dieron importancia singular a los orígenes.
  - El origen como contenedor de los elementos esenciales.
    - Los individuos se vuelven intercambiables.
    - Los primeros filósofos como planteadores de problemas eternos, simples
      por no estar aún cargados de lastres históricos.
=> Problemático:
  - Quizá las ideas solo son aprehensibles en la madurez de su desarrollo.
  - Quizá no se puede juzgar por sus orígenes.
  - Quizá que la evolución no es adhesión sucesiva de elementos accidentales.
  - Quizá el resumen de la evolución de una especie en el desarrollo de un
    individuo sea algo muy diferente.

La enseñanza también es selección forzosa.
  - Solo es posible aprender por partes.
  - Doble criterio de selección:
    1. Historiográfica.
    2. Didáctica.
    => Plantea conflictos.

La concepción historicista fomenta iniciar el estudio con la Historia de la
filosofía en su totalidad.
  - Fuerza una reducción a sus cumbres:
    - Elegir textos.
    - Facilitar textos con traducciones o comentarios.
  - O busca una unidad orgánica en las obras:
    - Esfuerzo de reconstrucción.
    - Selección forzosa de fragmentos que muchas veces van en contra 
      la organicidad.
=> Pese a todo hay un límite actual:
  - La explicación es iluminada por las ideas actuales.
  - La iluminación solo son en partes.
  => El historicismo es comprensión del presente por el pasado.
    - Naturaleza del hombre: ser en instantes hasta el instante actual,
      ser lo que ha sido.
    => Historicismo actual.

## ¿Son filosóficos nuestros días?

Desde el origen de la filosofía ha sido también la filosofía de la filosofía.
  - Los filósofos tienen una idea de la filosofía, por la cual siguen
    filosofando.
  - A su sombra también ha estado el escepticismo.
    - Discripencias de los filósofos.
  - Siempre se ha puesto en tela de juicio.

Desde la *Crítica de la razón pura* el ser de la filosofía se vuelve
un problema fundamental.
  - ¿Hasta cuándo? Es el propósito de este texto.

En nuestros días la filosofía es su historia como el hombre es su historia.
  - Tesis nuclear del historicismo.
  - La historicidad como una primera razón de ser de la filosofía de 
    la filosofía.
  - El valor y el poder de la filosofía es su segunda razón.
    - ¿Puede o no algo la filosofía?
  - El entusiasmo es una tercera razón.
    - En la madurez existe un quiebre.
  - La obstinación es la cuarta distinción.
    - Persistencia en lo desvalorado.
    - Mientras se razone seguirá habiendo filosofía.
=> Sendos momentos biográficos.
  - Afecta a la filosofía en su esencia.

La restauración de la filosofía como insistencia en la filosofía particular de
la humanidad.
  - Amputación de todo lo trascendente.
  - Visión del mundo moderno.
  - Inmanentismo reductivo.
  - Filosofía radicalmente moderna hasta su extremo o abismo.

Parece que la filosofía moderna en general ha buscado lo dado.
  - Contrasentido, lo dado no necesita buscarse, es anterior a lo que se busca.
    - Situación biográfica-histórica del filósofo.

En nuestros días la filosofía es paradójica:
  - Necesidad de una metafísica y no poder hacerla.

La filosofía viva requiere arrancarse del inmanentismo e irracionalismo.

## La situación de la filosofía en el momento presente

La filosofía quizá no es verdad objetiva pero es expresión de los tiempos.
  - Quizá la expresión más completa o radical.
  - Fruto de un cambio en el pasado.

Del pasado lo nos hace cargo del presente.
  - El presente apunta a una situación futura, para captar cabalmente el
    presente.
=> Naturaleza del tiempo y del hombre.

La filosofíaha sido dar razón deque los seres existen de una u otra manera.
  - En un Ser donde se identifican la esencia y la existencia.
=> En el pasado ha sido razón y teología.

La filosofía del tiempo presente:
  - Se adjetiva de «existencial».
    - Afirmación de los seres no tienen una última o primer razón de ser de
      sí u otros seres.
    - Lo que son los otros seres eslo que son para él.
    - Lo que él es, es lo que es para sí.
    - Flota sobre la nada.
    - La esencia como sin sentido.
  => Con la cancelación de la esencia, se elimina al Ser y solo queda la
     existencia.
    - Filosofía ya no como esencias, sino de existencias.
    - No es algo nuevo.

Lo más radical de la modernidad es la ciencia moderna.
  - Nace como hostil a las esencias.
  - Afanoso por dominar a los seres.
  - Hombre indiferente a Dios.
  - Filosofías materialistas y empiristas.
  - Filosfía existencialista, ateológica e irreligiosa.

La filosofía del momento presente:
  - En lo referente a la cosa religosa es la constitución de una existencia
    sin reminiscencia religiosa.
  - En lo referente a la cosa racional se presenta como irracionalista.
    - No es nuevo, la edad moderna es una crítica de la razón, desde el
      reconocimiento de sus límites hasta su anulación.

La filosofía existencialista lleva la modernidad hasta sus límites.
  - Ateológica, irreligiosa e irracionalista.
=> Estado de la filosofía del presente.

Para el futuro de la filosofía puede ser:
  - Autocrítica de la razón => filosofía de la filosofía.
  - Continuar siendo filosofía de esto o aquello.

Problema decisivo:
  - ¿La filosofía, razón y religión son solo etapas históricas o algo esencial
    a la naturaleza humana?

## Más sobre sociedad e historia

Respuesta a las objeciones del artículo «Sobre sociedad e historia».

La filosofía contemporánea indica que el hombre es historia.
  - Implica que cierta porción de la Humanidad no es histórica, no es humana.
    - No todos los hombres han sido hombres.
=> La división entre naturaleza y Humanidad es interna a la Humanidad.
  - vs igualdad humana.
  - La intención del artículo fue hacer notas estas consecuencias.
    - Hacer frence de las objeciones más sencillas a las más complejas.

La concepción de la historia está referido a la ciudad, «urbs».
  - Ciudad como sujeto cuando concurren dos elementos:
    1. Una determinada agrupación humana.
    2. Un cierto grado de civilización o cultura.
  - Dicotomía:
    * *Oppida* (bárbaros).
    * *Urbanitas* y *humanitas*.
  - «Urbs» abarca también esas formas de agrupación no perdurables que son
    «sedes» de una «cultura» superior.

Los bárbaros entran en la historia hasta cuando invaden.
  - Entrada a la historia, «historización».

Algunas partes no-históricas de la Humanidad han sabido, asistido, presenciado
o sufrido la historia.
  - Problema en la historización.
    - Lo que ha hecho es confinarlas en su naturaleza.

La historia no solo implica una estructura dinámica sino conciencia de una
estructura.
  - La estructura precisa de una determinada estructura.
  - La conciencia histórica es la relación entre las memorias individuales.
    - Expresión en la historiografía.
    - Diferencia al mito o leyenda.
    - Forma parte de la estructura histórica.

Dos tipos de historización:
  1. Pasiva: obligados a servir de instrumentos.
  2. Activa: bárbaros invasores.
=> Varias formas de historización:
  - Utilización instrumental.
  - Invasión.
  - Evangelización.
  - Colonización.
  - Civilización.
  => Movimiento integrante de la historia.

Contraargumento: no hay especies no-históricas sino con diferente tiempo.
  - vs:
    1. No hay evidencia de que pudo haber sido otro modo.
    2. La diferenca de tiempo constituye una distinción esencial, referente a
       lo espiritual.

Historia como historización progresiva de la Humanidad no-histórica.
  - Concepción dinámica.
=> vs: ¿Si es así o la historia son los actos?

La Humanidad como dualidad no-histórica e histórica es opuesta a la unidad 
del género humano.
  - La unidad parece tener fundamentos naturales, biológicos y psico-lógico-
    espirituales.
  - Quizá la dualidad no es esencial.
=> La diferencia es política.
  - Pasión política como creación de la historia.

## Sobrelos deberes sociales y el derecho ala libertad del filósofo

Dos cuestiones:
  1. ¿Debe el filósofo de filosofar sobre cuestiones sociales?
  2. ¿Debe el filósofo de participar en cuestiones sociales?

Primera cuestión:
  - A favor: solo si es su vocación.
  - En contra: puede no tener la suficiente preparación de aconsejar a 
    los demás.

Segunda cuestión:
  - Es la tendencia y ha sido históricamente favorable.

El intelectual y el filósofo siempre está comprometido por su sola actividad
intelectual.

El filósofo no tiene ningún derecho de expresión especial al del intelectual,
pero si al del ciudadano común si su actividad es para el bien común.

## Textos sobre el concepto de filosofía

### Dr. Larroyo

1. La filosofía es perfectible.
2. El órgano de la filosofía es la historia de la filosofía.
3. La filosofía como saber radical.
  - Indaga en el ser y no-ser como ser en sí.
    - Es ser en la razón.
    - Gaos comete el error de oponer al individuo de la comunidad.
    - El individualismo como [ilegible] fruto de la modernidad                 <
4. El progreso de la filosofía comprueba el carácter comunal de esta.
  - Son esfuerzos humanos.
5. Gaos comete el error de tratar a la conciencia como algo en sí,
   cuando es intencional: supone algo.
6-8. El pensar existe en función de su contenido.
9-10. La filosofía busca los primeros principios de la realidad.
11. Gaos niega el historicismo al decir que filosofar es discrepar de los
    demás ya que esto es solipsismo.
12. La definición de Gaos de la filosofía es de raíces heideggerianas.
  - No es concepto sino valoración del mundo.
13. La filosofía es obra comunal.

### Resumen de las tesis

1. Larroyo: la filosofía es perfectible.
2. Gaos: no conceptualizada sino historiada.
3. Gaos: la filosofía es el estudio del ser en sí.
  - vs: La conciencia es individual pero su contenido es social.               <
    - vs: ser en sí es un ser sobre los demás.
      - vs: ser en sí entonces es «ser en relación a».
   Larroyo: Gaos al concebir a la filosofía como discrepancia niego lo que se
   ha entendido históricamente por filosofía [punto 2].

### Dr. García Bacca

1. No existe La Verdad, sino verdades concretas.
  - vs: verdad única, eterna y perenne.
2. No hay filosofía sino filosofías, lo cual saca a flote la pretensión de ir
   de las filosofías a la Filosofía.
  - Plan diabólico.
3. Diferencias entre filosofía y ciencia:
  1. La filosofía engloba todo,la ciencia solo engloba según cada ciencia.
  2. La filosofía sabe a dialéctica, la ciencia sabe a lógica deductiva.
    - Muchas dialécticasy solo una lógica.
4. La filosofía es pretenciosa.
=> Conclusiones:
  1. La multitud de las filosofías es porque somos hombres.
  2. La pretensión de funda en la pretensión de transfinitud del hombre.
  3. Por ser hombres la pretensión no puede ir más allá de eso.

Sobre Gaos:
  1. Su filosofía es auténtica.
  2. La soberbia que menciona Gaos es la pretensión de hacer Filosofía.
  3. Su ausencia hace deber muchas explicaciones.

### A Larroyo

1. No se hondó en la perfectivilidad de la filosofía que en todo caso parece
   problemática.
2. El examen de la posibilidad de una definición es para quienes niegan esa
   posibilidad.
3. Larroyo no distingue entre el objeto de crítica de Gaos y Gaos mismo.
4. Remite a 1.
5. Cartesiano solo porque Descartes es el que mejor descubre lo que es
   filosofía.
  - No ve oposición entre el ser en sí y el ser intencional, no se refiere a
    la relación con su objeto, sino del sujeto.
6-8. Le parece confuso.
9-10. La filosofía como fundamentación radical le parece soberbio.
11. No le parece negar el historicismo porque no niega que la filosofía también
    se entienda como realidad.
12. Heideggeriano solo relativo al concepto de la filosofía y a su 
    interpretación como en 5.
13. Que el espectador lo interprete.

### A García Bacca

1. Se agradece.
2. Parece [ilegible] en lo que le reprochó a Larroyo.
3-4. Gran intento de explicación del punto 3.

## Siete puntos sobre vida y filosofía

### 1

Solo puedo exponer mi filosofía mediante una autobiografía.
  - Encuentro con la filosofía a través de su historia.
  - La historia se enseña que cada filosofía tiene su definición de filosofía.

### 2

Del estudio se pasó a vivir la filosofía contemporánea.
  - Recepción:
    1. La filosofía como saber insuficiente.
    2. La vida filosófica sin satisfacciones radicales.

### 3

Las filosofías como visiones del mundo.
  - No teoría, sino autobiografía.
  - ¿Qué es la filosofía? => ¿Qué soy yo?

### 4

Plan de obra.

### 5

Mi vida como caso particular de nuestra vida.

### 6

Nuestra vida: de Grecia a la obstinación contemporánea.

### 7

La vida como inpiración y clave de todo.

## Trece puntos sobre el concepto de filosofía

1. Mientras haya filosofía habrá filosofía de la filosofía.
2. Se niega la posibilidad de definición.
3. Imposibilidad de una definición única de la filosofía.
  - vs: propuesta de indagar sobre esta supuesta imposibilidad.
4. La realidad auténtica de la filosofía es su historia.
5. Al parecer es historiar en lugar de definir.
6. El filosofar no es histórico, sino personal.
7. Todo hacer algo con la filosofía es filosofía.
8. La Humanidad ha evolucionado para reconocer a los individuos entre
   las comunidades.
  - Deja de vivir en común.
  - La filosofía es reconocerse como individuo.
    - A vivir en sí y por sí.
    - Implica aislamiento.
    => Autenticidad.
  - Estimación como superior.
    - Aunque exista el miedo de quedarse solo, una reliquia de la comunidad.
    => Soberbia.
       - Sin razón en la medida que el individuo no puede dejar de vivir
         en comunidad.
       - Evolución de la comunidad al inmanentismo.
9. Filosofía: segregación de la personalidad.
  - Dialéctica entre filosofía personal y filosofía.
10. Insuficiencia de la personalidad aislada.
11. Argumentos fundados en la historia de la filosofía y en la vida.
12. Objetivación de lo personal.
13. Cf. con Dilthey.
