# Filosofía e historia: una lectura política

* Entidad: 10
* Asignatura: 63047
* Grupo: T118
* Créditos: 4
* Tipo: Curso
* Campo: Filosofía de la cultura
* Profesor(es): Dr. Mario Magallón Anaya

Aquí está el respaldo del pad: [https://pad.kefir.red/p/63047_t124_curso_filosofia-e-historia-una-lectura-](https://pad.kefir.red/p/63047_t124_curso_filosofia-e-historia-una-lectura-).
