# Primer semestre (agosto-noviembre del 2017)

| Entidad | Asignatura | Grupo | Crd |   Tipo    |          Campo          | Nombre de la asignatura | Profesor(es) |
|---------|------------|-------|-----|-----------|-------------------------|-------------------------|--------------|
|   10    |   63044    | T112  |  8  | Seminario | Filosofía de la cultura | Mercancía, capital y cinematografía | Dr. Carlos Oliva Mendoza |
|   10    |   63047    | T124  |  4  |   Curso   | Filosofía de la cultura | Georges Bataille: saber y transgresión | Dr. Ignacio González Díaz de la Serna |
|   10    |   63047    | T118  |  4  |   Curso   | Filosofía de la cultura | Filosofía e historia: una lectura política | Dr. Mario Magallón Anaya |
