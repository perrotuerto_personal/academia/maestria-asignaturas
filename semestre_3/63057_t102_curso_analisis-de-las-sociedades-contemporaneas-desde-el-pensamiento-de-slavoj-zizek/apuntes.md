# 6 de agosto

marifloraguilar@gmail.com
lechavar@cinvestav.mx
kabirabad@gmail.com

Dicen que es el filósofo más traducido y leído del mundo.

Zizek tiene estudios profundos de:
  * Freud
  * Lacan
  * Hegel
  * Marx
=> Propuesta de nuevo pensamiento crítico.

Mariflor: el aporte más importante de Zizek es que da un paso más 
adelante que Lacan.
  - Muestra que el pensamiento lacaniano está ya disperso en la Filosofía.
  - Obliga a pensar de otra manera: *vs*. el sentido común.

Filosofía de Zizek: vamos a actuar, sin importar lo que pase.
  - Mejor un desastre que un desierto.
  - Abogado de la acción violenta.

Para entender a Zizek se tienen que ver los conceptos lacanianos.
  - Lo Real, lo Simbólico y lo Imaginario.

El trabajo final será un trabajo para un coloquio a realizarse en diciembre.

Introducciones a Zizek:
  - Agon Hamza, *Repeating Zizek*.
  - Chris McMillan, *Zizek and the Communist Strategy on the Disavowed
    Foundations of Global Capitalism*.

Repo con lecturas:
  - GitLab: http://alturl.com/5gxuk
  - GitHub: http://alturl.com/m2s2p
  - Gist:   http://alturl.com/yczfs

# 7 de agosto

Análisis de _Las mininas_ para entender lo planteado en _El estadío
del espejo_ de Lacan.
  - Texto clásico de Lacan.
  - Fundamental para Zizek junto con Hegel y Marx.

¿Qué se dice en _El estadio del espejo_?
  - Qué hay antes del sujeto.
  - Inicia entre los 6 y 8 meses.
  - El niño como cachorro humano presimbólico.
  - Su cuerpo le es ajeno y se identifica con el cuerpo de la Madre.
  - Tiene gestualidad extralingüística.
=> Al ver al espejo ve la representación de su cuerpo.
  - Proceso de diferenciación de la Madre.

Nudo borromeo:
  - Lo Simbólico; introyección; ideal del yo.
  - Lo Imaginario; proyección; yo ideal.
  - Lo Real.

Pequeño otro: los padres, quienes constituyen al niño.
Gran otro: relatos e instituciones.

Saussure:
  - Signo:
    * Significado.
    * Significante.

Lacan:
  - Cadena del significante:
    * Significante > Significado.
=> El sujeto siempre es un sujeto en falta.
  - No hay identidad, nunca acaba un proceso de identificaciones.
  - De yo ideal a yo ideal.
  - El sujeto está alieanado al deseo.

Como Zizek no toma al sujeto como ego, no es adecuado llamarlo
posmoderno.
  - Retoma fuertemente la sospecha.

Marx:
  - No es una dialéctica del amo y el esclavo, sino una lucha de clases.
  - Fetichismo.
  - Plusvalor:
    * Absoluto.
    * Relativo.

# 20 de agosto

No fui.

# 27 de agosto

«Luz apagada»: la imposibilidad de conseguir plenitud a partir de la razón.
  - No hay razón pura, las pulsiones la atraviesan.

<!-- Engels, *El origen de la familia, la propiedad privada y el Estado* -->

Para Lacan el inconsciente no está en las profundidades, sino a flor de piel.

Hay ética porque hay deseo.
  - Hay deseo porque hay Ley.
    * Hay Ley porque hay lenguaje y muerte.

Kant: razón pura.
  - Sade: pulsión pura.

Todo deseo es deseo del Otro.
  - Reconocimiento.
  - Se está moldeado.
  - Los deseos son heterónomos.

# 3 de septiembre

Hubo paro.

# 10 de septiembre

Hubo paro.

# 17 de septiembre

Hubo paro.

# 24 de septiembre

Estudiantes que se rebelan = fortalecimiento del amo.
  - Nuevo aire para el capital.
  - Los ilotas.
=> Fortalecimiento porque no se ocupa del fantasma.
  - Por la ilusión de que puede haber diálogo.
    * Fantasía del diálogo.

Discurso del amo
  - Deseo unidireccional del amo.
  - Amo toma las decisiones: su autoridad sin fisuras.
  - Siervo sabe hacer las cosas.
  - Consolidación del poder: masculino.

Discurso de la universidad
  - Actuar por el supuesto saber objetivo.
  - Los amos se esconden bajo el saber.
  - Consolidación del poder: masculino.
  - Quien se escuda en el saber solo muestra una impotencia de ser el
    amo.

Discurso de la histérica
  - Cuestionamiento al amo y al saber.
  - Discurso que más toca lo real.
  - Emergencia de lo real.
  - No hay producción sino pérdida.
  - No se puede estar neutral, obliga a tomar postura.
  - El problema: el berrinche que fortalece al amo.

# 1 de octubre

No fui.

# 8 de octubre

Explicación del pensamiento de Zizek a través de movimientos
sociales en América Latina durante la segunda mitad del siglo XX.

Deuda simbólica: nunca  puede ser pagada y la forma de suspendarla
es el suicidio.

# 15 de octubre

No fui.

# 22 de octubre

No fui.

# 29 de octubre

Benjamin, Zizek y von Trier coinciden en la hipocresía de la moral y de
la corrección moral.

Violencia divina: ligada al derecho; la victoria de los que vencen hecha
legal.

# 5 de noviembre

<!-- 8 cuartillas para el coloquio del 10 de diciembre, 12 - 20 hrs.
     - Psicoanálisis y PI
     - Entregar por lo menos un día antes -->

¿Qué quieres? => Pregunta para saber qué hacer; explicitar el deseo.

Los países elevan sus niveles de vida como medida 
contrarrevolucionaria.

<!-- ¿Un objeto de consumo que en cada reproducción disminuya su
     efectividad de acumulación de capital? -->

<!-- La edición con _software_ libre o la edición libre buscan dar
     solución al problema de la reproducción de los objetos y el
     conocimiento. No cuestiona de fondo el ecosistema de la producción
     de cosas y de conocimientos, o la manera en como nos constituimos
     a través de estos. Son un umbral, no un sendero. -->

<!-- El _software_, la cultura y la edición libres son un síntoma de 
     un padecimiento muy arraigado en nuestra cultura -->

Las tradiciones son necesarias para probar la universalidad de la idea.

<!-- Ponerle cara a los modos de producción y problematizarlos -->

La verdadera acción es cuando se deja de lado el discurso contemporáneo.

No se puede curar del capitalismo sin dolor.

<!-- Al atomizar y en ausencia de un programa fuerte se diversifica
     lo que estamos criticando -->

# 12 de noviembre

Por Lenin, Zizek despierta del sueño dogmático de la democracia.
  - Se va de lleno a la hipótesis comunista.

Zizek: vivimos en una época donde hay que replantear desde cero la
política de izquierda.

Si una revolucación no ataca doble, pierde triple.

# 19 de noviembre

No asistí.

# 26 de noviembre

No asistí.
