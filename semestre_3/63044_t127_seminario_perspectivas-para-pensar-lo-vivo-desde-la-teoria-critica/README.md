# Perspectivas para pensar lo vivo desde la teoría crítica: Arendt, Foucault y Butler

* Entidad: 10
* Asignatura: 63044
* Grupo: T127
* Créditos: 8
* Tipo: Seminario
* Campo: Filosofía de la cultura
* Profesor(es): Dra. María Antonia González Valerio y Dra. Rosaura Martínez Ruiz
