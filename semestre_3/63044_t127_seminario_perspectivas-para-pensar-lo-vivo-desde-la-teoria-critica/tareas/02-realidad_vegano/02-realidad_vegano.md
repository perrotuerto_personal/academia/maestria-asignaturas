# El principio de la realidad en el discurso «vegano»

\begin{flushright}

\footnotesize El desarrollo humano hasta el presente me parece no necesitar
de explicación distinta del de los animales, y lo que de impulso
incansable a una mayor perfección se observa en una minoría de
individuos humanos puede comprenderse sin dificultad como consecuencia
de la represión de los instintos, proceso al que se debe lo más
valioso de la civilización humana.

\textsc{Sigmund Freud}, _Más allá del principio del placer_.

\end{flushright}

\normalsize\noindent Desde hace unas décadas ha tomado relevancia un tipo
de dieta y de estilo de vida que se autoconcibe a favor de lo
vivo. El discurso con el que se funda apela a la dignidad animal,
la protección del medio ambiente, la equidad social o en pos
de la salud. Esta práctica tiene como principal elemento la negativa
al consumo de cualquier tipo de bien que implique la explotación
animal, el cual incluye a nuestra especie, o del medio ambiente.\footnote{
Hay muchas ideas y argumentos que no contienen referencia. Esto
no se debe a que son desarrollos enteramente propios de quien
escribe. Pero tampoco es porque se quiera invisibilizar sus orígenes.
El contenido tiene su principal base en discusiones con diversas
personas en colectivos «veganos» o en sus acérrimos críticos
durante los diez años en los que me he identificado con el discurso
del vegetarianismo estricto. Espero que sea comprensible la carencia
de citas ya que varios de los fundamentos de este discurso surgen
en su oralidad y divulgación a modo de memes. Un desarrollo con
un aparato crítico explícito aún está pendiente. } Este estilo
de vida es un vegetarianismo estricto autodenominado «veganismo».\footnote{
Con la finalidad de visibilizar el nexo entre el «veganismo»
y el vegetarianismo se preferirá el desuso del neologismo. Queda
en deuda el análisis sobre qué tanto el término «veganismo» tiene
la intención de marcar distancia de su origen histórico y si
en su lugar se perfila a modo de una entidad sin antecedentes
históricos claros. }

La práctica vegetariana no es una novedad. En diversas culturas
se ha presentado, tal vez no de forma exclusiva, pero sí como
parte de ciclos de gestión energética. La mayoría de los casos
los discursos que la respaldan han sido de tintes religioso, místico,
de cuidado de la salud o llana limitación energética.

El aumento del consumo energético y sus consecuencias han proliferado
otro discurso para este tipo de prácticas. Por coherencia discursiva
al parecer alimentada por la visibilización de estos efectos,
este tipo de vegetarianismo ha perdido flexibilidad con el tiempo.
Pasando por la negativa a comer carne, continuando a la evasión
de comer cualquier producto de derivación animal, hasta un completo
cambio para evadir la reproducción de cualquier tipo de explotación
animal o medioambiental: alimentos, calzado, ropa, transporte,
trabajo, lenguaje, etcétera.

El discurso incluso parece agitarse mediante la creación de nuevos
neologismos que lo diversifica. Estudios científicos recientes
_según_ sugieren que el reino vegetal también «sufre» \parencite{dove2014a}.
La práctica se extiende para pretender abarcar la abolición de
esa clase de sufrimiento.

No importa qué tanto se violente el cuerpo o se degrade la salud,
este estilo de vida desde hace ya unos años forma parte del núcleo
de diferentes movimientos autodenominados radicales. Aunque en
los hechos por lo general existe un amplio margen entre lo que
se dice y lo que se hace, esta falta de congruencia no parece
afectar el núcleo del discurso. Al contrario, lo amplifica porque
otorga material para darle continuidad a sus ideas pilares.

Un par de estas ideas parecen de una obviedad que no ha despertado
interés el problematizarlas a fondo:

* La vida tiene un _valor intrínseco_ trascendente al ser humano.
* La vida es un _derecho_ sin importar la especie.
* El paradigma de lo vivo es la _vida digna_.
* La muerte es lo _opuesto_ a la vida.
* Las únicas muertes políticamente correctas son la _muerte
  digna_ o «_natural_».
* La muerte no es deseable al menos que sea el único _medio_
  para cesar con el sufrimiento.
* La vida _por sí misma_ ha de devenir en muerte.
* Toda vida y muerte que no sean dignas son una forma de _crueldad_.
* La vida es _comienzo y fuerza_, la muerte es _fin y flaqueza_.

\noindent Cualquier elemento que atente a uno de los puntos 
anteriores implica «especismo» que por su antropocentrismo inherente
no solo es desaconsejable, sino despreciable. Esto vuelve polémico
el intento de hacer ciertas precisiones que puedan desembocar
a una reflexión más a fondo de los fundamentos de este discurso.
Por ejemplo:

* El reino de los valores al menos tiene una estrecha relación
  con la especie humana y su cultura; no es claro cómo defender
  un valor más allá de esta esfera sin caer en recursos igualmente
  problemáticos.
* Todo derecho implica obligaciones por parte de un individuo
  que está al tanto de su sujeción a ciertas leyes, cultura
  y sociedad que funcionan para regular la convivencia; ¿cómo es posible un
  sujeto con derechos si no cuenta con la posibilidad de que
  al menos en el futuro comprenda lo que son las obligaciones?
* Históricamente existen indicios para pensar que la vida digna
  era poco común, propia de ciertas clases sociales acomodadas;
  parece conflictivo expandir ese modo de vida por su coste
  energético, más cuando se usa como medida de contrainsurgencia:
  pocos están dispuestos a revueltas que pongan en juego su
  dignidad o confort.
* La muerte no puede comprenderse sin la vida y viceversa;
  quizá más que oponerse, se complementan para englobar lo que
  entendemos como ser vivo. 
* La muerte digna también parece ser exclusiva de ciertas clases,
  así como no hay elementos suficientes como para aclarar qué
  se entiende por muerte «natural».
* Los medios se llevan a cabo en vida, si la muerte es su opuesto,
  hay una contradicción ya que no puede emplearse como medio,
  sino a lo sumo como el fin de esos medios.
* La vida por sí misma al parecer es una sustantivación de una
  acción primaria, el vivir, que ofusca más que es clara la relación
  con la muerte.
* Si en este contexto la dignidad es antónimo de la crueldad,
  cabría pensarse qué tanto los medios para una vida o muerte
  digna requieren la vida o muerte cruel de los «sin-voz». Entendiéndose
  por «voz» a una reducción y sinonimia de las lenguas humanas.
* La vida como comienzo y fuerza así como la muerte como fin
  y flaqueza puede basarse en modos de comprender al mundo y
  de una escatología donde el ser vivo es una singularidad temporal
  lineal que se vale del mismo recurso de otro tipo de escatologías:
  no puede refutarse ni verificarse, sino creerse, suponerse
  y _vivirla_.

\noindent Con estos recursos hay material suficiente
para amplificar la dieta al quehacer político. Una particularidad
de este reciente discurso es que a la par que fundamenta ciertas
prácticas relativas a la alimentación, sus ideales lo proyectan
a la discusión pública, a modificaciones en la legislación, a
interpelar otros modos de vida y, en general, a imponer o disuadir
una agenda política.

La alimentación, considerada cuestión privada excepto en tiempos
de guerra, se convierte en punta de lanza para la destrucción
de un sistema que da preferencia a la acumulación de capital y
que atenta a la vida. El anhelo por lo que se lucha es la gestación
de un mundo sin crueldad que en clave política es una cultura
donde no se dé primacía a la economía monetaria o financiera.

Pero no elimina de todo el interés por lo económico. Muchos de
los argumentos a favor de esta dieta interpelan al despilfarro
de recursos que conlleva los modos de vida comunes y dietas hegemónicas.
La preocupación económica es de tal grado que se busca un modo
de ser lo más directo, simplificado y eficiente posible.

La satisfacción reside en expandir este tipo de economía de pequeñas
comunas al globo entero. La insatisfacción se constituiría como
ese vivir que de manera indirecta, a través de la privación de
placeres, solo inducen al displacer. Es recurso común que este
discurso evidencie la baja calidad de vida y la muerte que provoca
la reproducción del capital.

De manera poco frecuente esta discursividad explicita el lugar
desde donde se produce y reproduce su discurso. En su vertiente
política se sitúa en la izquierda progresista hasta la plena
conciencia anarquista. Desde el aspecto geográfico su densidad
es relativa al desarrollo urbano. En cuanto al control de los
medios de producción, pulula más entre personas que no se dedican
a la industria agrícola o de productos animales que funcionan
como el principal medio para satisfacer las demandas energéticas de
nuestra especie. Para el imaginario de la lucha entre mundos
con modos opuestos de vida, se percibe en la civilización y no
en la barbarie, ya que la cadena masiva alimentaria requiere
de una plena conciencia y no un simple quehacer. Se trata de
un discurso con una complejidad autoconcebida como superior,
haciéndolo ciego a varios datos supuestos que se justifican como
hechos.

Lierre Keith, feminista radical estadunidense y que durante décadas
fue defensora del «veganismo», es una de las críticas ante la
supuesta superioridad de este discurso. En _The Vegetarian Myth_
comienza por las figuras comunes para la defensa del «veganismo»:
los datos científicos \parencite{keith2009a}. Poco a poco y mediante
otros estudios argumenta que este tipo de dieta es todo lo contrario
a lo que dice ser: no es sustentable, no defiende la vida,
es conservador, es hermético y, al final, no sacude de manera
radical al mundo en el que vivimos \parencite{keith2009a}.

En lugar de continuar el debate sobre este terreno, se propone
a lanzar una tesis muy arriesgada. Si el «veganismo» pretende
eliminar la crueldad. Si la crueldad es innata al capitalismo.
Si el capitalismo se ha expandido por medio de la guerra —aunque
sea comercial—. Si la militarización solo puede ser alimentada
por medio de la densidad poblacional. Si la civilización es
la única en poder de proveer los insumos básicos para la guerra.
Si la civilización tiene su corolario en la creación de urbes.
Si las ciudades no producen sus propios alimentos así como requieren
de una gran cantidad de insumos. Si la caza y los
productos animales históricamente han sido insuficientes para satisfacer
esta demanda. Si la agricultura es la única técnica que ha podido
producir de manera artificial los alimentos necesarios para la
civilización. Entonces la génesis de todos los problemas yacen
en una infraestructura básica para nuestra especie: la agricultura.
Para Keith, el «veganismo», en su pretensión de ser más que una
dieta, solo reproduce lo que ha jurado abolir así
como distorsiona lo que le es tan sagrado: la vida \parencite{keith2009a}.

La tesis no puede ser defendida con suficiencia por la cantidad
de premisas que supone. No obstante, en su camino devela ciertos
elementos relevantes de lo que Freud define como «principio de
la realidad» que «sin abandonar el propósito de una final consecución
del placer, exige y logra el aplazamiento de la satisfacción
[…], y nos fuerza a aceptar pacientemente el displacer […] para
llegar al placer» \parencite{freud2002a}. En este caso se gesta
en un discurso y acción cada vez más inflexibles que hacen evidentes
sus políticas conservadores ante la vida.

Keith argumenta que el discurso «vegano» es complaciente, proteccionista
e incluso paternalista \parencite{keith2009a} para los seres vivos
sin-voz. Ante la pretensión de una postura no antropocéntrica
de la vida, en lugar de borrar las _distinciones_ entre lo humano
y lo animal, las profundiza. Lo humano y lo animal se invisibilizaría
en la división entre seres vivos capaz de discurso y los que
no. Se arrolla por completo una dicotomía para dar paso a otra
que vuelve irreconciliable la posibilidad de su superación. Al
basarse en el lenguaje, este discurso termina por generar una
doble explicación para un mismo objeto de estudio: lo vivo. Por
un lado la especie humana con voz, del otro, el resto del reino
animal, vegetal y mineral. Esto borra las diferencias al homologar
el resto de los reinos en en un solo concepto: entes sin-voz.
En este sentido no sorprende que paulatinamente este discurso
se oriente no solo a defender la vida animal, sino también la
vegetal o incluso una defensa animalista del reino mineral: un
mundo lleno de «vida» sin nunca explicar qué se entiende por
ello.

Esta autora también habla del carácter _minoritario_ del movimiento
vegano \parencite{keith2009a} cuya génesis específica yace en núcleos
urbanos. La minoría puede delimitarse aún más si se toma en cuenta
que entre las ciudades, los lugares donde este discurso tiene
mayor recibimiento son en las urbes con influencia occidental.
Esto tal vez puede explicarse porque parte del discurso de la
modernidad acarrea ideas progresistas así como una idea ilustrada
de práctica aceptación: la vida como un sustantivo y con un valor
intrínseco y positivo. El carácter minoritario también se debe
a que requiere de características muy específicas para la reproducción
de su discurso:

* Una sociedad que tenga en alta estima la libertad de expresión
  individual.
* Una comunidad abastecida por la industria agrícola que dé acceso
  a alimentos sin importar la temporada.
* Una apertura política que permita corrientes progresistas
  que, sin atentar de fondo a la estabilidad estatal, ofrezca
  una distinción ante la vida «corriente» o «conservadora».
* Una capacidad de creación de mercados que se puedan beneficiar
  del discurso, como está siendo la constitución de la industria
  _verde_: una personalización de los medios de producción y
  reproducción ya existentes pero en su vertiente «amigable»
  con el medio ambiente y el reino animal.
* Un discurso que no teme en establecerse como oposición política
  aunque su fuerza no reside en su capacidad de acción, sino
  en la posibilidad de redirigirla para usarla como capital
  político de las instituciones ya existentes, como son los
  aparatos democráticos.

\noindent Además, Keith es muy enfática en cómo el supuesto _perfeccionamiento_
que ofrece el «veganismo» es una _represión_ de deseos
e incluso de un masoquismo que afecta principalmente a
las mujeres \parencite{keith2009a}. La sátira
tiende ha exhibirlo: la persona «vegana» por lo regular
hace trampa. Desde la necedad de obviar los hechos hasta
la constitución de una práctica normativa que a su
vez implica una consciente violación de sus normas.
Los objetivos pretendidos implican
un _aplazamiento de la satisfacción_ en pos de un placer que
no es obtenido de manera directa. Nadie puede, por mera voluntad,
eliminar la crueldad o destruir el capitalismo. Para conseguirlo
el discurso «vegano» tiene mano dura en sus
costumbres y en los modos de organización política para la consecución
de sus objetivos.

Sin embargo, los ideales políticos de este discurso no son alcanzables
con sus propios medios. Un mundo sin crueldad y sin acumulación
de capital exigen más que una extrapolación de los fundamentos
de una dieta. No solo es necesario usar lo vivo como estandarte,
hay que repensarlo y problematizar todos los elementos heredados
que generan supuestos de lo que es la vida, la muerte y su relación.

La limitación de su capacidad política provoca al menos dos consecuencias.
La primera es la _repetición_. La persona «vegana», ante el displacer
generado por la imposibilidad de concretar sus objetivos, comienza
a repetir una y otra vez el discurso y la acción que se ha mostrado
infértil. Sabe que no es suficiente, pero sin poder salir de
la frustración o del trauma, ejecuta una y otra vez las mismas
estrategias.

La otra, por el hastío y la desilusión de emprender caminos indirectos
que solo le generan displacer, pretende la satisfacción directa
de su deseo. Se trata de un tipo de estrategia política de «acción
directa» que ante los medios ha sido tildada de radical o terrorista,
aunque al final no tiene un impacto a mediano o largo plazo.
La liberación del ganado, el incendio de fábricas o el sabotaje
a la cadena de suministros no se ha mostrado eficaz para conseguir
la satisfacción. Incluso los aleja aún más, ya que genera un
displacer en la opinión pública hasta el punto de crear oposiciones.
Por ejemplo, el aumento del consumo cárnico como gesto
de disconformidad.

Al final, existen varios puntos débiles que impiden el advenimiento
de un «superhombre» \parencite{freud2002a}. Aunque también abre
nuevos caminos al convertir en lugar común una serie de ideas sobre
la vida y la muerte que se daban por sobreentendidas y esenciales.
Quizá el efecto positivo del discurso del vegetarianismo estricto
no sea la fiabilidad de lo que argumenta, sino la aproximación
al cierre de una manera de entender lo vivo que se ha expandido
de manera transversal a través de la civilización occidental
y sus periferias. Tal vez, a partir de la repetición o la acción
directa sea ahora posible replantear: ¿qué es al final la vida?
