<section epub:type="frontmatter" role="doc-frontmatter">

# Correspondencia perdida entre Arendt y Agamben {.centrado}

Ramiro Santa Ana Anguiano como Giorgio Agamben \ 
Ernesto Mañaga García como Hannah Arendt {.centrado}

</section>
<section epub:type="preamble" role="doc-preamble">

# Nota del editor

En febrero de 1970 el joven Giorgio Agamben, de tan sólo 27 años, le 
escribió a Hannah Arendt, con ya 63 años de edad. Su entusiasmo es 
claro, Agamben admite la influencia de Arendt y el goce por haber 
conocido sus libros a partir de Dominique Fourcade, quien conoció en 
los seminarios de Heidegger entre 1966 y 1968
@parencite[artilleria2016a].

Sin embargo, no podemos decir lo mismo de Arendt. Adjunta a esa carta 
Agamben le hace llegar un ensayo «sobre la violencia», a lo que Arendt 
le responde que le tomará un tiempo leerlo por estar escrito en 
italiano para finalizar con un agradecimiento por su entusiasmo
@parencite[artilleria2016a].

Esta correspondencia tan breve nos llamó la atención. Por este motivo 
le encomendamos a Ernesto Santa Ana que ahondara un poco al respeto. 
Esta pequeña antología contienen la traducción tres cartas adicionales 
de su intercambio epistolar en inglés. Esperamos que sea del agrado 
del lector.

Ramiro Magaña \
Octubre del 2018, \
Ciudad de México {.espacio-arriba1 .derecha}

</section>
<section epub:type="chapter" role="doc-chapter">

# Carta de Agamben del 11 de marzo de 1970

Roma, 11 de marzo de 1970 {.derecha}

Querida Sra. Arendt: {.espacio-arriba2 .sin-sangria .izquierda}

Primero que nada quiero disculparme, no estaba al tanto de su 
italiano, ojalá mi redacción no se lo complique. Ahora en adelante
procuraré escribir en inglés ya que al parecer así no tendremos 
problemas en comunicarnos. {.espacio-arriba2 .sin-sangria}

Estaba esperando con ansias su respuesta. Le escribo hasta ahora 
porque hay otro tema que quiero platicarle y durante días busqué la
manera de abordarlo en una carta sin que se prestara a un mal 
entendido. La extensión y el formato de una epístola en ocasiones no
permiten a uno expresarse de la misma manera como es posible en un
ensayo…

Entonces, la lectura de _La condición humana_ me ha provocado pensar 
en la relación entre el hombre y el animal. Espero que esto no la tome 
por sorpresa, porque no me parece que la intención de su obra sea 
ahondar en ese aspecto.

Desde el prólogo usted menciona unas ideas muy potentes: «las cosas 
que deben su existencia exclusivamente a los hombres condicionan de 
manera constante a sus productores humanos». Casi de manera inmediata
también escribe: «Resulta muy improbable que nosotros, que podemos
saber, determinar, definir las esencias naturales de las cosas […],
seamos capaces de hacer lo mismo con nosotros mismos».

Con esto dilapida la idea de que es posible que nosotros podamos
aprehender lo que es nuestra «naturaleza» (si es que semejante cosa es
posible). Pero esto también hizo preguntarme, ¿qué tanto la 
búsqueda por saber qué es el hombre no depende de lo que nos une o
distingue de los animales?

A este respecto me parece que su obra es un magnífico despliegue que
va de lo que nos hace común a los animales (el _animal laborans_);
pasando por un principio de diferenciación que envuelve a la noción
de mundo como una producción humana a través de objetos (lo que llama
_homo faber_); hasta llegar a lo propiamente humano: la acción del
_zõon politikon_.

He de admitir que me dejó perplejo, no había dado con semejante 
traslado. Por lo general, la tradición humanística ha empleado al ser
animal como un principio de comunión o de diferenciación para la
definición del ser humano. Nunca había leído que alguien intentara
crear un nexo entre ambas variantes.

Permítame explicarme. De manera esquemática me parece que en nuestra
tradición de pensamiento se empezó a pensar el ser del hombre a partir
de una artículación entre un elemento animal y otro sobrenatural,
social o divino. De hecho, usted menciona la sospecha de usar 
entidades «superhumanas» para poder dar con la «naturaleza humana». No 
sólo eso, también describe ---en mi opinión de manera muy acertada--- 
cómo de modo equívoco nuestra tradición de pensamiento trasladó el 
término _zõon politikon_ al de _animal socialis_. Esto indujo a una 
generalización de lo «social» hasta el punto de verlo como una 
característica de lo humano, cuando en la Antigüedad la socialibilidad
del hombre se consideraba un elemento en común con el animal.

Con esto, usted retoma estas comunes dicotomías de nuestra tradición y
las anula sin la necesidad de investigar _el misterio metafísico_ de
dicha conjunción. Con un análisis tan sucinto elimina toda posibilidad
de seguir pensando la diferencia entre lo animal y lo humano desde
terrenos metafísico o teológico, para que en su lugar se trabaje desde
una perspectiva política. Tengo que felicitarla por eso, me parece
simplemente fascinante.

Sin embargo, temo que esto poco a poco conlleva a indagar el ser del
hombre desde el _misterio práctico y político_ de esta diferenciación.
Su análisis del _homo faber_ hizo recordarme una y otra vez las 
lecturas que he hecho sobre el concepto heideggeriano de mundo. 
¡Cuánta similitud! ¡Cuánto le debemos a Heidegger este nuevo horizonte 
para pensar el ser!

Pero he de admitir que me causa desasosiego que para la noción de 
mundo como artificio humano y la relación interconstitutiva haya 
empleado el término _homo faber_. ¿Sabía usted que el género _homo_ es 
muy conflictivo dentro de la taxonomía?

El género _homo_ desde muy temprano se empezó a utilizar en nuestra
tradición para englobar todo aquel animal semejante al hombre. No 
obstante, una vez realizada la familiarización, para poder distinguir
al ser humano del resto se buscó un elemento diferenciador. Éste no 
vino del campo de las ciencias naturales, sino de un elemento común a
nuestra tradición: la racionalidad.

En poco tiempo se localizó semejante atropello. El _logos_ pende de la 
capacidad del hombre para articular el lenguaje. Es decir, el lenguaje 
funciona como un principio diferenciador entre el hombre y el animal. 
Sin embargo, el lenguaje es al mismo tiempo un producto del ser 
humano. ¿Cómo es posible que algo sea principio al mismo tiempo que un 
producto del mismo ente que pretende fundar?

Por desgracia temo que este problema poco a poco ha sido ocultado por
las teorías evolutivas del hombre. En mi opinión pienso que es muy
curioso. Muchas de estas teorías buscan una definición de nuestra
especie sin apoyarse en nuestra tradición metafísica y teológica. 
Pero en su pretensión y en la incapacidad de poder explicar de manera
contundente el salto cualitativo entre el hombre y el resto de los
primates, sin notarlo han terminado por recurrir en justificar dicho
salto con un elemento tan bien incrustado en nuestra tradición 
filosófica, la sublime la racionalidad.

Esto me hace concluir de modo tentativo que el género _homo_ y la
especie _homo sapiens_ no están definidos de manera clara. En su 
lugar, me parece que es una especie de artificio cuyo objetivo es
_producir el reconocimiento de lo humano_. He pensado en fijar este 
fenómeno en un concepto que sería la «máquina antropológica». En mi
opinión este intento no obedece a la «naturaleza» del hombre, sino
al intento de producir una explicación de la génesis de su propio ser.
¿Qué opina usted al respecto? ¿Qué tan equívoco podría ser su concepto
de _homo faber_?

En este sentido he empezado a leer los cursos de Heidegger del 
semestre invernal de 1929 y 1930. Observo cierta posibilidad de salir
de este circulo a través del pensamiento heideggeriano sobre lo 
abierto. Aunque aún ignoro si no es un nuevo esfuerzo que sucumbe a lo 
mismo o se trata de un intento de una superación dialéctica del
problema. Por eso también he estado tentado a revisar de nuevo a 
Walter Benjamin porque al parecer tiene _una imagen completamente
distinta de esa relación_.

Pero antes de avanzar, quisiera saber que opina usted sobre mis 
preocupaciones fruto de su obra…

Cordialmente tuyo, \
Giorgio Agamben {.espacio-arriba2 .sin-sangria .derecha}

</section>
<section epub:type="chapter" role="doc-chapter">

# Carta de Arendt del 30 de marzo de 1970

30 de marzo de 1970 {.derecha}

Sr. Giorgio Agamben \
Piazza delle Coppelle 48 \
Roma, Italia {.espacio-arriba2 .sin-sangria .izquierda}
 
Querido Sr. Agamben, {.espacio-arriba2 .sin-sangria .izquierda}

He recibido con mucha alegría su carta en la que expone
sus ideas acerca de la biopolítica, en especial sobre
la máquina antropológica. Quiero felicitarle sus planteamientos
refinados y originales; sin embargo, me he dado la
licencia de hacerle algunas observaciones sobre sus
conceptos, con la finalidad de que el diálogo sea fructífero
y acorde con la verdad. {.espacio-arriba2 .sin-sangria}

Como ha dicho que ha leído mi obra _La Condición Humana_,
no lo aburriré repitiendo cosas que ampliamente conoce,
sólo esbozaré algunas ideas expuestas en ella que me
ayudarán a situar su propuesta, por lo que primeramente
me gustaría plantear de forma concisa el problema del
_homo faber_ y, después, añadir algunas palabras sobre
lo que pienso de su crítica a la máquina antropológica.

## 1) Crítica del hombre como trabajador (_homo faber_)

En la modernidad, la premisa cartesiana de que sólo
se puede tener certeza incontrovertible de lo construido
intelectualmente por el hombre ha impulsado el excesivo
desarrollo del _homo faber_. El intelectualismo moderno
ha provocado la exacerbación de la capacidad humana
para construir una realidad con base en la racionalidad
de medios y fines.

Desde la modernidad, el trabajo ha sido la garantía
y la certeza de que el mundo está hecho a la medida
de los hombres, por lo que su importancia ha sido mayúscula,
a tal punto que se ha considerado que la esencia de
los hombres radica en la transformación de la naturaleza.
Para la modernidad, el trabajo es la esencia de los
hombres. Cuando se lee con atención a los pensadores
más representativos de la filosofía moderna, se puede
reconocer esta aseveración. Karl Marx, por ejemplo,
ha dicho que: «Es en su trabajo sobre el mundo objetivo
como el hombre se muestra realmente como ser genérico.
_El objeto del trabajo es, pues, la objetivación de
la vida del hombre como especie»_ @parencite[fromm1962a].

El desmesurado fomento de la capacidad para transformar
el mundo ha devenido en un fenómeno contrario a las
características del _homo faber_, el ser transformador
de su entorno. El efecto del trabajo en la realidad
humana no ha sido la constitución de un mundo común
firme, sino el advenimiento y triunfo del _animal laborals_.
El excesivo afán de modificar la realidad ha provocado
que la única aspiración de los hombres sea servirse
de los productos humanos, construidos con la técnica,
para conservar su vida. Me permito citar una parte
de mi obra: «Quizá nada indica con mayor claridad el
fundamental fracaso del _homo faber_ en afirmarse como
la rapidez con que el principio de utilidad, _la quintaesencia
de su punto de vista sobre el mundo desapareció y se
reemplazó por el de mayor felicidad del mayor número»_
@parencite[arendt2006a].

La angustia que provoca en los pensadores marxistas-hegelianos
la condición paupérrima del ser humano en las sociedades
de consumo, la semejanza que hay entre las personas
y los animales en la fase actual de la historia tiene
su raíz en la premisa moderna que ellos mismos han
aceptado: que el hombre es fundamentalmente un trabajador.
El asombro de Kojéve ante esos _humanoides_ que sólo
consumen y aspiran a una felicidad vulgar ha sido provocado
por el excesivo impulso del trabajo en la modernidad.

La modernidad olvidó que la verdadera prerrogativa
del ser humano no es el trabajo, sino la acción: la
capacidad que tienen los hombres para incidir en el
dominio público de forma conjunta y consensuada. Este
ocultamiento de la modernidad ha sido el catalizador
de las sociedades de consumo y de la reducción de la
política en administración de masas, cuya única aspiración
es la felicidad (autoconservación).

Ante esta catástrofe histórica, su propuesta de la
suspensión de la máquina antropológica está muy alejada
de la verdad y tiene consecuencias aún peores que el
problema de la reducción de la política en administración
de masas. La alternativa a las consecuencias del olvido
de la condición humana original es la memoria de que
la actividad más noble y elevada del ser humano es
la acción. Rescatar y repensar este concepto nos ayudará
a mostrar la falsedad del mundo moderno.

## 2) Crítica a la máquina antropológica

De acuerdo con sus palabras, todo el pensamiento occidental
está basado en un artilugio metafísico falso y de consecuencias
terribles en la política: la máquina antropológica.
La metafísica ha sido un mecanismo que articula arbitrariamente
las características humanas y animales para determinar
lo que debe ser en cada caso el hombre y el animal.
Esta articulación siempre es producto de una decisión
arbitraria, por lo que sólo tiene realidad en el interior
de los hombres. Usted sostiene que: «_La división_
[…] _en animal y humana pasa entonces, sobre todo, por
el interior del viviente hombre como una frontera móvil_;
sin esta íntima cesura, probablemente no sea posible
la decisión misma sobre lo que es humano y lo que no
lo es» @parencite[agamben2006a].

Esta decisión metafísica es un experimento para determinar
lo que debe ser el hombre y su relación con la vida
sensitiva y animal, por lo que tiene repercusiones
en el papel que ocuparán los seres humanos en la esfera
política. La _polis_ es el núcleo en el que se manifiesta
este artilugio metafísico, por lo mismo en ella se
presentan con mayor atrocidad las consecuencias funestas
de la dialéctica ficticia entre el hombre y el animal.
Para usted, «_la política occidental es co-originariamente
biopolítica_» @parencite[agamben2006a].

En la política moderna está presente esta captura de
la _vita_, como lo muestra la gestión integral de la
vida humana efectuada por la maquinaria estatal que
ha derivado en sociedades de masas cuya única aspiración
es la autoconservación, así como las atrocidades de
los totalitarismos del siglo +++XX+++. Estos dos fenómenos
aparentemente distintos e inconexos provienen de una
misma raíz: la política como biopolítica.

Si se analizan los campos de concentración y exterminio
del Nacional Socialismo, se verá que en estos laboratorios
se llevó a cabo cínicamente el experimento de decidir
qué debe ser entendido como un hombre y que lo que
es lo que debe ser relegado a estatus de simple vida
animal. Lo citaré nuevamente, para usted: «_Tal vez
también los campos de concentración y de exterminio
sean un experimento de este género, una tentativa extrema
y monstruosa de decidir entre lo humano y lo inhumano_»
@parencite[agamben2006a].

Estimado Agamben, usted sostiene que esta tensión entre
el hombre y el animal ha sido la causante de los problemas
de la política (que van desde la gestión integral vida
hasta la aniquilación de la vida desnuda en los totalitarismos
del siglo +++XX+++), ya que la esfera jurídica decide lo
que en cada caso debe ser un hombre, así como sus derechos
dentro del Estado, sin embargo, en esta decisión también
está implícito lo que no debe ser respetado como un
hombre, sino como un animal, una vida que es posible
sacrificar sin cometer crimen, una vida desnuda. Es
por esta razón que no ve con buenos ojos la función
del Estado y su mecanismo para decidir lo que es el
hombre y el animal.

Su propuesta no es seguir sosteniendo la máquina antropológica,
sino suspenderla. Para usted, lo que se debe hacer
no es buscar cómo se vincula lo humano y lo animal,
sino cómo se puede desarticular este falsa relación.
Nos dice: «En nuestra cultura, el hombre ha sido siempre
pensado como la articulación y la conjunción de un
cuerpo y un alma, de un viviente y de un logos, de
un elemento natural (o animal) y de un elemento sobrenatural,
social o divino. _Tenemos que aprender, en cambio,
a pensar el hombre como lo que resulta de la desconexión
de estos dos elementos y no investigar el misterio
metafísico de la conjunción, sino el misterio práctico
y político de la separación_» @parencite[agamben2006a].

Con la finalidad de que la política no capture la vida
humana y traiga como consecuencia la instalación permanente
de los humanoides consumistas, así como de las atrocidades
criminales de los totalitarismos, nos propone desmantelar
el artilugio de la máquina antropológica. Su posición
intelectual es: «_Volver inoperante la máquina que
gobierna nuestra concepción del hombre significará,
por lo tanto, ya no buscar nuevas articulaciones, sino
exhibir_ […] _el hiato que separa ---en el hombre--- el hombre
y el animal_, arriesgarse en este vacío: suspensión
de la suspensión, _shabbat_ tanto del animal como del
hombre» @parencite[agamben2006a].

De acuerdo con sus planteamientos, el ejercicio filosófico-político
de desmantelar la máquina antropológica es el único
camino viable para desactivar (volver inoperante) la
potencia dominadora del Estado moderno sobre los hombres.
Sin embargo, sostenemos que usted ignora que su propuesta
no es más que una de las caras del triunfo del _animal
laborans_ como lo hemos descrito en _La Condición Humana,_
por lo que su aspiración no cambia el camino catastrófico
de la biopolítica, sino que lo acelera. Su propuesta
es el desarrollo de las premisas de los entusiastas
defensores del _homo faber_.

El artilugio de la máquina antropológica consiste en
suponer veladamente un elemento humano en el animal
(todavía no humano), lo cual abre un espacio de indeterminación
en los dos elementos que se confunden. Esta suspensión
de lo humano y lo animal es análogo al concepto de
Estado de excepción en la teoría jurídica-política,
ya que en éste el derecho se confunde con el hecho,
sin que se sepa en ningún momento cuál es cuál.
{.espacio-arriba1 .sin-sangria}

Para usted, la máquina antropológica debe ser desactivada
mediante la suspensión de la relación entre lo humano
y lo animal, con la finalidad de que la potencia catastrófica
de la dialéctica se detenga. La suspensión de la suspensión
significa: evitar el agregado del elemento humano en
el animal (todavía no humano), no definir ni delinear
a la humanidad con elementos que suponemos en la naturaleza.
Lo que propone es detener la dialéctica del hombre
con el animal, dejar suspendido el momento en que se
produce lo humano en su diferencia con lo animal; sin
embargo, en este esfuerzo lo humano se convierte en
algo indeterminado, sin rostro, por lo mismo el ejercicio
termina por no tener sentido, pues lo que quería ser
custodiado y salvado termina por no existir, es como
querer rescatar el agua de un vaso con nuestras manos
y comprobar inmediatamente que nuestro objeto rescatado
se diluye en nuestras manos.

El desmantelamiento de la tensión entre el hombre y
el animal equivale asumir que no existen realmente
estos dos elementos de la lucha, por lo que implícitamente
se acepta que sólo hay alguna «materia indistinguible»
en eso que hemos creído milenariamente como una tensión
de dos polos. La desconexión del elemento humano de
lo animal, para no producir lo humano nos deja en un
sitio indeterminado, ya que no se sabe lo que queda
cuando lo que _creíamos_ humano se convierte en otra
cosa distinta así misma. Usted mismo sabe que su ejercicio
práctico-político lleva a la indistinción entre el
hombre y el animal. Nos dice: «La máquina antropológica
no articula más naturaleza y hombre para producir lo
humano a través de la suspensión y la captura de lo
inhumano. _La máquina se ha detenido, por así decir,
está “en estado de suspensión” y, en la recíproca suspensión
de los dos términos, algo para lo que quizás no tengamos
nombres y que no es ya ni animal ni hombre, se instala
entre la naturaleza y humanidad_» @parencite[agamben2006a].

El resultado de la suspensión es que el concepto de
lo humano como algo positivo deja de tener significado,
ya que la desarticulación, la desconexión del elemento
«x» del animal, no puede ser llamado humano, a riesgo
de cometer un error lógico o decir una perogrullada.
Ahora bien, si no hay más que «materia homogénea»,
un elemento neutro que no es el hombre pero tampoco
el animal, entonces no se alcanza a ver por qué tendríamos
que aspirar a liberar a los hombres y los animales
de la opresión de la biopolítica. Con la erradicación
de la humanidad, se elimina también el concepto de
dignidad humana, el cual inspira a los hombres a liberarse
de la opresión con la finalidad de perfeccionarse.
Recordemos la metáfora del agua rescatada con las manos.

Esta concepción de la materia caótica y con-fundida
es un aspecto del triunfo del _animals laborans_ en
la modernidad, ya que como sabemos en la actualidad
la victoria de la labor (sobre el trabajo y la acción)
ha provocado que el ser humano sea concebido como mero
mecanismo sin propósito, indistinguible de la naturaleza,
así como sujeto al dominio y a la manipulación. Su
aspiración de suspender la suspensión de la máquina
antropológica se manifiesta con fuerza en el triunfo
del _animal laborans_, porque sólo en un mundo en el
que la técnica tiene tanta fortaleza para convertir
la realidad en un espacio efímero, transitorio y sin
sentido es posible que aparezca un humanoide que sólo
tenga por finalidad la mera reproducción de su vida,
sin preguntarse por el domino desmesurado sobre la
naturaleza.

En realidad, su posición no es una alternativa a los
problemas históricos, culturales y políticos nacidos
en la modernidad, sino un reflejo del triunfo de la
labor sobre el trabajo y la acción, tal como lo he
expuesto en _La Condición Humana_. El excesivo desarrollo
del trabajo y la técnica ha transformado a los hombres
en entes consumistas, sin aspiraciones legítimas y
trascendentes, que no se pueden distinguir de los mecanismos
orgánicos, porque son puro movimiento circular y perenne.
En el mundo de la técnica, los seres humanos se transforman
en materia neutra e indistinta, tal como en la suspensión
de la suspensión.

El triunfo del _animal laborans_ tiene semejanzas con
el concepto de _homo prima_ de Günther Anders. La exacerbación
de la técnica, la racionalidad de medios y fines, termina
por convertir en materia de dominio al propio sujeto,
al ser humano. Ahora el ser humano es un producto de
la técnica. El filósofo Günther Anders percibe este
proceso de cosificación. Él «anticipa que ante la nueva
revolución tecnológica […] el ser humano se escindiría
en dos nuevas figuras ontológicas: sería, por lado,
un _homo creator_ capaz de transformar todas las cosas
de modo sustancial, incluyéndose a sí mismo; pero,
por otro lado, se convertiría en un no menos insólito
_homo materia_, es decir, en “_materia prima_ [Rohstoff]
de sus propias producciones tecnológicas”. El ser humano
se convierte así en el principal objeto de transformación»
@parencite[linares2008a]. Ilustre Agamben, cuando niega que exista 
en sí mismo una distinción entre el hombre y el animal, convierte
a los seres humanos en materia moldeable de la metafísica,
los termina convirtiendo en _homo prima_.

En el fondo, reproduce subrepticiamente el argumento
de los entusiastas defensores del _homo faber_ (sean
marxistas, hegelianos o tecnófilos), ya que resulta
que para usted la naturaleza humana sólo es un sustrato
de dominio y la realidad está al servicio de la transformación,
por lo que no tiene ningún compromiso sobre lo que
es lo humano y lo animal.

Como dijimos más arriba, el camino no es negar que
exista la distinción entre lo animal y lo humano, sino
en recuperar que lo que lo distingue no es el trabajo,
sino la acción. Recordar que lo que la modernidad olvidó
es la senda que puede poner freno al triunfo de la
técnica y del _animal laborans_. Las categorías de
_La Condición Humana_ no son una estructura rígida
y definitiva, sino conceptos que sirven para iluminar
experiencias antiguas que la tradición filosófica han
ocultado, por lo mismo, la categoría de acción no debe
pensarse como un destino inexorable de los hombres,
ya que resultaría ingenuo creer esto en un mundo administrado
por el Estado nación, sino como una guía para resistir
a los problemas que nos plantea la política oficial.

La alternativa para salir del solipsismo moderno (germen
de los males que nos aquejan actualmente) es aferrarnos
a la vivencia originaria de que los seres humanos somos
esencialmente plurales; desde siempre, nacemos condicionados
por la presencia de otros seres humanos que nos interpelan,
que nos hablan y nos escuchan, por lo que no podemos
ignorarlos y perdernos en el laberinto de la subjetividad.

La pluralidad es la condición del acercamiento de los
seres humanos para discutir sobre los asuntos comunes
y llegar acuerdos acerca del modo más conveniente para
resolverlos. En estos acercamientos dialógicos se revela
la condición más propia de los seres humanos, porque
el discurso hace patente las certezas interiores de
los hombres y las convierte en realidades colectivas
que sirven para fundar relaciones en el dominio público.

Sólo con la acción y el discurso aparecemos en el mundo
y, particularmente, en la palestra de los asuntos colectivos,
por lo que nuestra existencia física y particular tiene
sentido para nosotros mismos y para los demás. En mi
obra he mencionado: «Con la palabra y el acto nos insertamos
en el mundo humano y esta inserción es como un segundo
nacimiento, en el que confirmamos y asumimos el hecho
desnudo de nuestra original apariencia física»
@parencite[arendt2006a].

Estimado Agamben, esperemos que la humanidad que, en
los albores de la modernidad, se encerró en el solipsismo
sea capaz de salir de él mediante la acción en común
y que pueda decir en voz alta lo expresado por Rubén
Darío, ilustre poeta nicaragüense:
{.espacio-arriba1 .sin-sangria}

_La torre de marfil tentó mi anhelo; \
quise encerrarme dentro de mí mismo. \
Y tuve hambre de espacio y sed de cielo \
desde las sombras de mi propio abismo._
{.espacio-arriba1 .sin-sangria .centrado .poema}

Con gran estima, {.espacio-arriba1 .sin-sangria}
 
Tuya, \
Hannah Arendt {.espacio-arriba2 .sin-sangria .derecha}

</section>
<section epub:type="chapter" role="doc-chapter">

# Carta de Agamben del 30 de abril de 1970

Roma, 30 de abril de 1970 {.derecha}

Querida Sra. Arendt: {.espacio-arriba2 .sin-sangria .izquierda}

¡Nunca deja de sorprenderme, Sra. Arendt! Cuando recibí su carta
tuve que suspender todas mis actividades. He de admitir que me ha 
costado trabajo entenderla. Probablemente me tomé meses, sino es que 
años, poder comprender cabalmente lo que me ha escrito…
{.espacio-arriba2 .sin-sangria}

Aprecio mucho su crítica a aquella «cosa» que he llamado
«máquina antropológica». Por desgracia, le escribo con prisa, además
de que temo que el espacio no sea suficiente para poder contestarle. 
Ahora mismo me encuentro en plena mudanza, así como tendría que 
extenderme demasiado si le escribo por este medio.

Me gustaría mucho poder platicar con usted, ¿no tiene planeada
un viaje por Europa? Por fortuna aún soy relativamente joven y no 
tengo problemas con desplazarme y hacer del tren mi habitación.

Cordialmente tuyo, \
Giorgio Agamben {.espacio-arriba2 .sin-sangria .derecha}

</section>
<section epub:type="epilogue" role="doc-epilogue">

# Epílogo

Por iniciativa de Ramiro Magaña, me dediqué a buscar en los archivos 
de Hannah Arendt su intercambio epistolar con Giorgio Agamben,
resguardados en la Biblioteca del Congreso de +++EE. UU.+++ 
@parencite[tercera2018a].

Aunque breve —¿qué tantó influyó la mudanza que hizo Agamben?—, 
es interesante percibir en estas cartas cómo Abamben ya estaba 
trabajando con el concepto de «máquina antropológica». Por lo 
que puede leerse, existen ciertas diferencias en la concepción inicial 
---y un tanto burda--- de este concepto. Al parecer, la crítica de 
Arendt le ayudaría a Agamben para reelaborarlo y hacerlo público en 
2002.

Con este pequeño trabajo, espero que el lector pueda tener más 
elementos que le ayuden a analizar la genealogía de la 
«máquina antropológica».

Ernesto Santa Ana \
Octubre del 2018, \
Washington D. C. {.espacio-arriba1 .derecha}

</section>
