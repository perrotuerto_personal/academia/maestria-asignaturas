# Tercer semestre (agosto-noviembre del 2018)

| Entidad | Asignatura | Grupo | Crd |   Tipo    |          Campo          | Nombre de la asignatura | Profesor(es) |
|---------|------------|-------|-----|-----------|-------------------------|-------------------------|--------------|
|   10    |   63057    | T102  |  4  |   Curso   | Filosofía de la cultura | Análisis de las sociedades contemporáneas desde el pensamiento de Slavoj Zizek | Dra. Mariflor Aguilar Rivero y Dra. Laura Echavarría Canto |
|   10    |   63044    | T127  |  4  | Seminario | Filosofía de la cultura | Perspectivas para pensar lo vivo desde la teoría crítica: Arendt, Foucault y Butler | Dra. María Antonia González Valerio y Dra. Rosaura Martínez Ruiz |
