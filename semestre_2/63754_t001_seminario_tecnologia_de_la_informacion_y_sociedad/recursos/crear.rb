#!/usr/bin/env ruby
# encoding: UTF-8
# coding: UTF-8

Encoding.default_internal = Encoding::UTF_8

require 'fileutils'

# Obtiene el número de escrito donde ha de empezar la compilación
indice = ARGF.argv[0].to_i - 1

# Para obtener todos los archivos con escritos
archivos = []

# Va al directorio de «recursos»
Dir.chdir(File.dirname(__FILE__))

# Elimina los archivos PDF previos
if indice <= 0
	puts "ADVERTENCIA: Eliminando PDF previos…"
	Dir.glob(Dir.pwd + "/../tareas/*.pdf") do |archivo|
		File.delete(archivo)
	end
end

# Obtiene los archivos con escritos
Dir.glob(Dir.pwd + "/../tareas/*") do |archivo|
	i = File.basename(archivo).to_i

	# Si es MD y tiene una numeración al inicio
	if File.extname(archivo) == ".md" && i > 0
		archivos.push(archivo)
	end
end

# Los ordena alfabéticamente
archivos = archivos.sort

# Itera cada archivo
archivos.each_with_index do |archivo,i|
	# Solo si se trata de un escrito igual o mayor al índice deseado
	if i >= indice
		archivo_nombre = File.basename(archivo, ".md")
		titulo = ""

		contenido = []

		puts "\n\n----------------------------------------\nCreando PDF de «#{archivo_nombre}»…\n----------------------------------------\n\n"

		# Abre el archivo de Markdown para analizarlo
		md_abierto = File.open(archivo, "r:UTF-8") 
		md_abierto.each_with_index do |linea, j|

			# Función para sustituir versalitas
			def versalita texto
				regex = /\[(\w*?)\]\{\.vers.*?\}/
				captura = ""
				
				# Obtiene las capturas si se encuentra la expresión
				if texto =~ regex; captura = texto.scan(regex); end
		
				# Si hay capturas, hace la sustitución
				if captura.length > 0
					captura.each do |c|
						texto = texto.gsub("[" + c[0] + "]{.versalita}", "\\textsc{" + c[0].downcase + "}")
					end
				end
				return texto
			end

			# La primera línea es donde está el título
			if j == 0
				# Elimina espacios innecesarios y convierte las itálicas
				titulo = linea.gsub(/#\s+/, "").gsub(/\s+$/,"")
				titulo = titulo.gsub(/\*(.*?)\*/, "\\textit{" + '\1' + "}")
			end

			# Cambia las versalitas y las citas entre paréntesis
			linea = versalita linea
			linea = linea.gsub(/--parencite\((.*?)\)--/, "\\parencite{" + '\1' + "}")
			contenido.push(linea)
		end

		# Guarda el nuevo archivo
		md_nuevo = File.open(archivo_nombre + "_tmp.md", "w:UTF-8")
		md_nuevo.puts contenido
		md_nuevo.close

		# ¡Por fin Pandoc! OJO: directamente no funciona así que hay que crear un TeX
		system("pandoc #{archivo_nombre}_tmp.md --pdf-engine=xelatex --template=template.latex -V lang:es -V documentclass:book -V papersize:a5 -V classoption:oneside -V geometry:margin=1in -V indent:true -V title:\"#{titulo}\" --bibliography=bibliografia.bib --biblatex -o #{archivo_nombre}.tex")

		# Generación del archivo a partir del archivo TeX
		system("biber -q #{archivo_nombre}")
		system("pdflatex -synctex=1 -interaction=batchmode #{archivo_nombre}.tex")
		system("biber -q #{archivo_nombre}")
		system("pdflatex -synctex=1 -interaction=batchmode #{archivo_nombre}.tex")
		system("pdflatex -synctex=1 -interaction=batchmode #{archivo_nombre}.tex")

		# Mueve el PDF a la carpeta «pdfs»
		FileUtils.mv(archivo_nombre + ".pdf", "../tareas/" + archivo_nombre + ".pdf")

		# Elimina los archivos creados
		Dir.glob(Dir.pwd + "/*") do |archivo|
			if File.basename(archivo) =~ /^#{archivo_nombre}/
				File.delete(archivo)
			end
		end
	end
end
