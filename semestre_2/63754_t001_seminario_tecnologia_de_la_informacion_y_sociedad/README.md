# Tecnología de la información y sociedad

* Entidad: 10
* Asignatura: 63754
* Grupo: T001
* Créditos: 4
* Tipo: Seminario
* Campo: Tecnología de la información y sociedad
* Profesor(es): Dra. Georgina Araceli Torres Vargas

Aquí está el respaldo del pad: [https://pad.kefir.red/p/63754_t001_seminario_tecnologia_de_la_informacion_](https://pad.kefir.red/p/63754_t001_seminario_tecnologia_de_la_informacion_).
