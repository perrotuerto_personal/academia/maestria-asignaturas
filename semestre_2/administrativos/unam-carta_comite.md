\begin{flushright}

24 de enero de 2018 \\
Ciudad de México

\end{flushright}

\vskip 3em

Comité Académico \newline
Programa de Maestría y Doctorado en Filosofía \newline
Universidad Nacional Autónoma de México

\vskip 3em

Por medio del presente solicito al Comité Académico poder cursar una asignatura 
de «Tecnología de la información y sociedad» de la Maestría en Bibliotecología y 
Estudios de la Información de esta misma facultad. Esta asignatura sería para cubrir 
los créditos de la materia optativo y, al tratarse de un seminario, cuenta con más
créditos de los necesarios. Mi tutor, Ernesto Priani Saisó, está de acuerdo con
la pertinencia de esta asignatura para mi tesis de investigación. Además, Juan
José Calva González, Coordinador del Posgrado en Bibliotecología y Estudios de
la Información ha verificado que es posible inscribir la materia.

Mi proyecto de tesis versa sobre la propiedad intelectual y su impacto cultural,
principalmente en las comunidades científicas y filosóficas. Uno de los ejes
de investigación es sobre la influencia que han tenido las nuevas tecnologías
de la información y la comunicación en el quehacer cultural, y cómo esto ha
sido comprendido para la modificación y creación de nuevas leyes entorno a la
propiedad intelectual. Estos cambios no solo se da en un plano jurídico o social, 
sino también filosófico, ya que las justificaciones de las diversas manifestaciones 
de la propiedad intelectual son de índole histórico-filosóficas. Por estos motivos,
mi tutor y yo consideramos que la asignatura «Tecnología de la información y sociedad» 
es afín a mi tema de investigación, además de que permite una mayor
interdisciplinariedad en mis estudios de posgrado.

Por mi parte es todo. Hasta luego.

\begin{center}

\vskip 3em

Ramiro Santa Ana Anguiano \\
Estudiante de la Maestría en Filosofía \\
Número de cuenta: 518012285

\vskip 3em

Ernesto Priani Saisó \\
Profesor de la Facultad de Filosofía y Letras

\end{center}
