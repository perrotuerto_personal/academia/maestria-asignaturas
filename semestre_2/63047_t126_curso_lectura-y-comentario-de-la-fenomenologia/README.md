# Lectura y comentario de La fenomenología del espíritu de Hegel

* Entidad: 10
* Asignatura: 63047
* Grupo: T126
* Créditos: 4
* Tipo: Curso
* Campo: Filosofía de la cultura
* Profesor(es): Dra. Virginia López-Domínguez

Aquí está el respaldo del pad: [https://pad.kefir.red/p/63047_t126_curso_lectura-y-comentario-de-la-fenome](https://pad.kefir.red/p/63047_t126_curso_lectura-y-comentario-de-la-fenome).
