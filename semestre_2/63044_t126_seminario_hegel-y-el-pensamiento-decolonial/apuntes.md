# 31 de enero

Para el profesor el libro más importante de Hegel es la
*Ciencia de la lógica*.
  * Fundamental para entender su sistema.
=> Enfrentar a Hegel conel pensamiento decolonial.
  * Hegel como símbolo del «mal» de la modernidad.

Analizar tres críticas a Hegel.
  1. Historicismo.
  2. Colonialismo.
  3. Racismo.

Conclusión que se quiere llegar: es posible hablar de progreso,
pero de modo negativo; como capacidad autocrítica.

Hegel como pensador posmetafísico, más cercano al siglo XX.
  * Hegel como apologista del Estado prusiano es falso.

En Platón ya hay una crítica al relativismo, mostrando su
petición de principio (*Teeteto*).
  * Acontece algo semejante en la crítica al progreso del
    decolonialismo, para criticar el progreso se requiere:
    1. Tener una preconcepción del concepto de «progreso».
    2. Tener una idea de lo que está bien y mal.
=> Una apología.

*Teeteto* es el diálogo sobre el conocimiento.
  * Enfoque a la primera definición: conocer es percibir.
    * No hay criterios para decir que es válido o no.
    * La cosa es lo que es y no es.

Hegel quiere tirar por la borda una realidad objetiva para ejercer
la crítica.

# 7 de febrero

Llegué tarde, a la mitad de la sesión.

Para Spninoza los textos religiosos no dive qué es verdadero o falso.
  - Solo indica cómo vivir y obedecer a Dios.
  - La obediencia se acota a amar y ayudar al prójimo.
=> Si lleva a otro compartamiento, no está justificado.
  - Hay espacio a la fe, pero no es necesaria.
=> Con la razón se puede llegar al mismo resultado, pero son pocos
   los virtuosos.
  - La fe es conveniente para llegar a la vida moral.
  => La fe es funcional, es útil.
=> Creer en Dios o la razón ayuda a limitar la libertad para respetar 
a los otros.

La razón permite darse cuenta qué nos determinó.
  - No es la razón como autodeterminación.

Spinoza y Hegel se asemejan en tratar de internalizar dualismos en
una unidad.
  - En Hegel la novedad de la modernidad es una negatividad.
    - La relatividad como estaticidad, no hay relación entre uno y otro.
    => No hay espacio para crítica.

Dado <=> Puesto
Heteronomía <=> Autonomía
=> Contradicciones internas que permite la dinamicidad al único sistema.

<!-- Próxima sesión: se sigue con Condorcet -->

# 14 de febrero

Condorcet: la historia es un solo proceso donde cabe cualquier
comunidad humana.
  * La especie humana como sujeto histórico.
  * La perfección como idea regulativa que lucha contra los prejuicios.

*¿Qué es la Ilustración?*
  * «Atrévete a pensar».
    * No es pensar solo.
    * Es enfrentar tu propia razón con la de otros.
  * Distinción entre el uso público y privado de la razón.
    * Público: se puede dar opinión.
    * Privado: se debe de obedecer.
  => Inversión de lo que comúnmente se entiende por público y privado.
    * Lo público se tiende a entender como esfera normativa institucional.
    * Lo privado se tiende a entender como relativo al individuo.
  => ¿Qué seguridad que nuestra opinión sea certera?
=> Optimismo ante la razón.

Minoría de edad: incapacidad de servirse uno mismo.
  * Autoculpa, no es debido a otros: cobardía, facilidad o pereza.
  * Ilustrado: pensar por sí mismo y lanzarlo a la voz pública.
    * La razón es una cuestión intersubjetiva: social.

# 21 de febrero

Plática sobre Blumenberg.
  - El pensamiento moderno tiene una falsa idea de sí mismo.
    - No es novedoso, solo «secularización» de ideas teológicas.
    => El «progreso» es una secularización escatológica.

Las filosofías de la historia no son parte constitutivas de la modernidad.
  - Intentan responder una pregunta heredada de la época cristiana.
  - Es posible hablar de modernidad y progreso sin esto.
=> Es posible un futuro como constante redefinición.

En el concepto de «secularización» es sustancialista.
  - La continuidad en la historia no es con sustancias, sino en problemas.
    - Pero no tiene que ser así.

<!-- Mi exposición sobre Chakrabarty -->

Chakrabarty: si la modernidad fuera capaz de cotemporalidad, podría 
quitarse el esquema de progreso-atraso.

Quijano: la globalización es la cúspide de la colonización.

El sistema-mundo moderno se articula en el «descubrimiento» de América.
  - El capitalismo es la nueva estructura del control del trabajo.
  - La raza como naturalización de las relaciones de dominación.
=> Raza como primer criterio para la distribución mundial.
  - Su relación con el capital es cuando la dominación pasa a ser
    explotación.

# 28 de febrero

Dussel: el otro es necesario para entender la subjetividad moderna, 
el yo.
  * El *ego cogito* tiene una voluntad de poder sobre el otro.
  * Salvar la razón como proceso crítico.
=> vs: la igualdad entre los otros que busca Dussel viene de la 
   modernidad europea.
  * Es algo abstracto que permite lo concreto (la convivencia).
  * Busca rescatar el concepto de la modernidad, independientemente de
    su gestación histórica.

Tesis fuerte: el caso de América y los crímenes cometidos por los
colonizadores fue un acontecimiento necesario para la modernidad
europea.

Tesis suave: históricamente el caso de América permitió la modernidad
europea; sin embargo, es posible rescatar sus ideales y aplicarla
«correctamente».

# 7 de marzo

<!-- Mi exposición sobre Hegel y su lucha de la Ilustración
     contra la superstición -->

La lucha contra la Ilustración está dividido en tres partes:
  1. Lo que la Ilustración cree es distinta de la creencia de la fe.
  2. Lo que de hecho ambas comparten.
  3. Lo que en verdad las distingue.

Primero: la Ilustración se enfrenta a la fe.
  - La fe se rinde ante la tradición.
  - La razón y la fe tienen fundamentos completamente diferentes.
  - La fe asume verdades sin razonar.
  - La fe llega a una esencia absoluta.
  - La razón tiene criterios, no esencias absolutas.
=> Supone un estado sin presuposiciones, la Ilustraciones se ve como
   neutral.

Segundo: el criterio de verdad se vuelve la relacionalidad de las 
cosas, por lo que el absoluto sí tiene un contenido utilitarista.
  - El cotenido es la percepción sensible.
  - La intelección no es solo negatividad, también es posivitidad.
  - La intelección y la fe hacen lo mismo: ponen algo como criterio.
  - La intelección da su propia autoridad, como la fe.
=> Solo ha intercambiado una aseveración por otra.

Tercero: la Ilustración es vanidosa porque cree que la verdad la 
extrae de sí misma.
  - La fe no es el absolutamente otro.
  - La razón tiene una capacidad negativa de crítica que una vez 
    empezada ya no es rechazable.
=> Rescatar el carácter negativo e históricamente dinámico.

Kant: yo trascendental.
Hegel: nosotros histórico.

Ilustración negativa: hacer ver lo «dado» como «puesto».

# 14 de marzo

No vine.

# 21 de marzo

No hubo clases.

# 28 de marzo

Vacaciones.

# 4 de abril

Hegel: en el derecho natural del siglo XVI y XVII no está implícita la
propiedad privada.

# 11 de abril

En HDRio2018.

# 18 de abril

Mi cumpleaños, no vine.

# 25 de abril

Parágrafos sobre el racismo y la barbarie en la *Filosofía del 
derecho*:
* 41-44
* 57
* 66
* 93 => El fundamental
* 248 (adición)
=> Los demás son añadidos después y se sacó los parágrafos de la 
   *Enclicopedia*
* 18 (adición)
* 123 (adición)
* 139 (adición)
* 187 (comentario)
* 194 (comentario)
* 350-351

# 2 de mayo

No vine.

# 9 de mayo

No vine.

# 16 de mayo

Como ser natural no se puede hablar de libertad o esclavitud.

La diferencia entre razas es por su ubicación geográfica, no por
la raza en sí misma.
  * La ubucación geográfica determina las capacidades de cada una.

El sexo y la raza no se puede justificar (para bien o para mal).

Ser libres un estatus que se ha de adquirir y que requiere del otro.

# 23 de mayo

Foucault sobre la Ilustración y la modernidad.
  - Criticar al concepto del sujeto: es contingente e histórico; por
    lo tanto, mudable e innecesario.
  - Genealogía: muestra lo necesario como contingente.
  - Arqueología: muestra lo universal como particular.
