# Hegel y el pensamiento decolonial: crítica como progreso

* Entidad: 10
* Asignatura: 63044
* Grupo: T126
* Créditos: 8
* Tipo: Seminario
* Campo: Filosofía de la cultura
* Profesor(es): Dr. Luis Guzmán

Aquí está el respaldo del pad: [https://pad.kefir.red/p/63044_t126_seminario_hegel-y-el-pensamiento-decolo](https://pad.kefir.red/p/63044_t126_seminario_hegel-y-el-pensamiento-decolo).
