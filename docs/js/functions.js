var values = [
  {
    label: "Todos los semestres (porcentaje)",
    data: [53.49,23.26,10.85,12.40]
  },
  {
    label: "Semestre 1 (porcentaje)",
    data: [55.56,24.44,11.11,8.89]
  },
  {
    label: "Semestre 2 (porcentaje)",
    data: [54,18,16,12]
  },
  {
    label: "Semestre 3 (porcentaje)",
    data: [50,29.41,2.94,17.65]
  }
];

function build (e, i) {
  new Chart(document.getElementById("chart" + i), {
    type: 'doughnut',
    data: {
      labels: [
        "Asistencia",
        "Inasistencia por mi culpa",
        "Inasistencia por el docente",
        "Inasistencia por otros motivos"
      ],
      datasets: [
        {
          label: e.label,
          backgroundColor: [
            "#630A9A",
            "#CD5018",
            "#F36404",
            "#FE7002"
          ],
          data: e.data
        }
      ]
    },
    options: {
      title: {
        display: true,
        text: e.label
      },
      legend: {
        display: false
      },
      plugins: {
        datalabels: {
          color: 'white'
        }
      }
    }
  });
}

window.onload = function () {
  for (var i = 0; i < values.length; i++)
    build(values[i], i);
};
